

export const logout = () => {
  
  localStorage.removeItem("ChynarProfile");
};

export const isLogin = () => {
  if (localStorage.getItem("ChynarProfile")) {
    var data = JSON.parse(localStorage.getItem("ChynarProfile"));
    if (data.token) {
      return true;
    } else {
      // localStorage.removeItem("sebetProfile");
    }
  }
  return false;
};

export const isLoginAdmin = () => {
  if (localStorage.getItem("ChynarProfile")) {
    var data = JSON.parse(localStorage.getItem("ChynarProfile"));
    if (data.permission && data.token) {
      return true;
    } else {
      // localStorage.removeItem("sebetProfile");
    }
  } else {
    return false;
  }
};
