import React, { useContext, useEffect, useState } from "react";

import {Link, useHistory} from "react-router-dom"
import "../../css/style.css";
import serpay from "../../img/main/cynar.png"
import { axiosInstance, BASE_URL } from "../../utils/axiosIntance";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { SebedimContext } from "../../context/Sebedim";

const Brends = ()=>{

  const { dil } = useContext(SebedimContext);
  const [brands,setBrands] = useState([]);
  const history = useHistory();

  useEffect(()=>{
    getBrands();
  },[])
  const getBrands = ()=>{
    let welayatID = localStorage.getItem("welayatId")
    axiosInstance.get("/api/brand/kategory/"+welayatID).then((data)=>{
      setBrands(data.data);
      
    }).catch((err)=>{
      console.log(err);
    })
  }
    return(
        <main className="main">
            <section className="main-company-block">
            
              {brands?.map((brands,i)=>{
                return <React.Fragment> 
                        {brands?.Brands.length>0 && <h1 style={{textAlign: "left",padding: "10px 11%",fontSize: "24px"}}>{i+1}. {dil==="TM"?brands.name_tm:(dil==="RU"?brands.name_ru:brands.name_en)}</h1>}
                        
                      < div className="main-company-block__container __container">
                        <section className="main-company-block__row">
                          {brands?.Brands?.map((brand)=>{
                            console.log(brand)
                          return brand.active && <Link
                          // to="/brand"
                          onClick={()=>{localStorage.setItem("brandId",brand.id);history.push({pathname:"/brand/"+brand.id})}}
                          className="main-company-block__box "
                        >
                          {/* <img src={BASE_URL+"/"+brand.surat} alt="brand surat" /> */}
                          <LazyLoadImage
                            alt={"surat"}
                            src={BASE_URL+"/"+brand.surat} 
                            />
                        </Link>
                        })}
                        </section>
                        </div>
                </React.Fragment>
              })}


              
              {/* {
               
               markets && markets.map((market,i)=>{
                 return market.Markets.map((market1,j)=>{
                  
                    return <Link onClick={()=>GoToMarket(market1.id)} to={`/market/${market1.id}`} className={`main-company-block__box ${j===0 && i===0 && "main-company-block__box--big"}`}>
                            <img src={BASE_URL +"/"+ market1.surat} alt="" />
                          </Link>
                          })
                  })
                  
              } */}
            {/* </section> */}
          {/* </div> */}
        </section>
        </main>
    )
}

export default Brends;