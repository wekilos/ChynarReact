import React, { useContext, useEffect, useState } from "react";

import {Link, useHistory, useParams} from "react-router-dom"
import "../../css/style.css";
import serpay from "../../img/main/cynar.png"
import Cart from "../../components/cart";
import { axiosInstance, BASE_URL } from "../../utils/axiosIntance";
import { SebedimContext } from "../../context/Sebedim";

const BrendProducts = ()=>{

  const { dil } = useContext(SebedimContext);
  const {brandId} = useParams()
    const history = useHistory();
  const [brandProducts,setBrandProducts] = useState([]);

  useEffect(()=>{
    getBrandProducts();
  },[])
  const getBrandProducts = ()=>{
    // let brandid = localStorage.getItem("brandId");
    let welayatId = localStorage.getItem("welayatId");
    axiosInstance.get("/api/products",{
       params:{ welayatId:welayatId,
        brandId:brandId}
    }).then((data)=>{
      console.log(data.data);
        setBrandProducts(data.data);
    }).catch((err)=>{
      console.log(err);
    })
  }
    return(
        <main class="main">
        <section class="favorite">
          <section class="favorite__bred-crumb bred-crumb">
            <div class="bred-crumb__container __container">
              <div class="bred-crumb__links">
                <Link to="/home" class="bred-crumb__link">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="15"
                    height="15"
                    viewBox="0 0 15 15"
                  >
                    <path
                      id="noun_Home_2102808"
                      d="M7.028,12.906l.327-.276v7.745A.625.625,0,0,0,7.98,21H19.02a.625.625,0,0,0,.625-.625V12.63l.327.276a.625.625,0,0,0,.806-.955l-3-2.533V7.039a.625.625,0,1,0-1.25,0V8.363L13.9,6.147a.625.625,0,0,0-.806,0l-6.875,5.8a.625.625,0,1,0,.806.955ZM12.1,19.75V15.238H14.9V19.75ZM13.5,7.443l4.895,4.132V19.75H16.147V14.613a.625.625,0,0,0-.625-.625H11.478a.625.625,0,0,0-.625.625V19.75H8.605V11.575Z"
                      transform="translate(-6 -6)"
                    />
                  </svg>
                </Link>
                <Link onClick={()=>{history.push({pathname:`/market/${localStorage.getItem("MarketId")}`})}} class="bred-crumb__link"
                  >{dil==="TM"?"Çynar market":(dil==="RU"?"Чынар маркет":"Cynar market")}
                </Link>
                <Link to="/brends" class="bred-crumb__link"
                  >{dil==="TM"?"Brendler":(dil==="RU"?"Бренди":"Brends")}
                </Link>
                <Link to="#" class="bred-crumb__link"
                  >{dil==="TM"?brandProducts[0]?.Brand?.name_tm:(dil==="RU"?brandProducts[0]?.Brand?.name_ru:brandProducts[0]?.Brand?.name_en)}
                </Link>
              </div>
            </div>
          </section>
          <section class="favorite__products products">
            <div class="products__container __container">
              <section class="products__list">
                <section class="product">
                  {
                    brandProducts.map((brand)=>{
                      console.log(brand)
                      return brand.is_active &&  <Cart product={brand} />
                    })
                  }
                </section>
              </section>
            </div>
          </section>
        </section>
      </main>
    )
}

export default BrendProducts;