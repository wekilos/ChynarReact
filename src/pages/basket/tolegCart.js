import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { SebedimContext } from "../../context/Sebedim";

import "../../css/style.css"

import product from "../../img/products/1.png" ;
import { BASE_URL } from "../../utils/axiosIntance";
const TolegCart = (props)=>{

    const {dil } = useContext(SebedimContext)
    console.log(props.product)
    const [product,setProduct] = useState(props.product);
    return (
        <article class="basket-mini__column">
            <div class="basket-mini__img">
            <img src={BASE_URL+"/"+props.product.surat}/>
            </div>
            <div class="basket-mini__content">
            <Link href={`/product/${props.product.id}`} class="basket-mini__title"
                >{dil==="TM"?props.product.name_tm:(dil==="RU"?props.product.name_ru:props.product.name_en)}</Link>
            <div class="basket-mini__key"># {props.product.id}</div>
            </div>
            <div class="basket-mini__price">{product.is_sale?
            (product.is_valyuta_price?product.sale_price*product.Config.currency_exchange:product.sale_price)
            :(product.is_valyuta_price?product.price*product.Config.currency_exchange:product.price)}  TMT</div>
        </article>
    )
}

export default TolegCart;