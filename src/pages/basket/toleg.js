import React,{useContext, useEffect, useState} from "react";
import { Link , useParams, useHistory} from "react-router-dom";

import "../../css/style.css";
import TolegCart from "./tolegCart";
import TolegAddress from "./tolegAddress";
import { axiosInstance } from "../../utils/axiosIntance";
import { SebedimContext } from "../../context/Sebedim";
import { message } from "antd";

const Toleg = (props)=>{

  const history = useHistory() 
  const {dil,RemoveAllPro} = useContext(SebedimContext);

  let { id, sum } = useParams();
    const [popUp,setPopUp] = useState(false);
    const [active1,setActive1 ]= useState("active");
    const [toleg,setToleg] = useState(true);
    const [market,setMarket] = useState({});
    const [jemi,setJemi] = useState(sum);
    const [products,setProducts] = useState([]);
    const [time,setTime] = useState(3);
    const [profile,setProfile] = useState(null);
    const [address,setAddress] = useState({});
    const [addresses,setAddresses] = useState([]);
    const [orderedProduct,setOrderedProduct] = useState([]);
    const [rec_name,setrec_name] = useState();
    const [rec_number,setrec_number] = useState();
    const [rec_address,setrec_address] = useState();
    const MarketId = localStorage.getItem("MarketId");

    useEffect(()=>{
      getMarket(id);
      let Userprofile = JSON.parse(localStorage.getItem("ChynarProfile"));
      console.log("Userprofile",Userprofile)
      setProfile(Userprofile);
      Userprofile && getAddresses(Userprofile.id)
      let pro = JSON.parse(localStorage.getItem("SargytProducts"));
      pro && setProducts(pro);
      console.log("proooooo",pro)
      let obj = []
      pro && pro.map((p)=>{
        obj.push({
          ProductId:p.product.id,
          amount:p.sany,
          razmer:p?.razmer,
          renk:p?.renk,

        })
      })
      setOrderedProduct(obj)
    },[])
    useEffect(()=>{
        popUp === true ? setActive1("active") : setActive1("");
        console.log(popUp)
    },[popUp]);

    const AddressHandler = (add)=>{
      setAddress({...address,rec_address:add.rec_address})
    }

    const getMarket = (id)=>{
      axiosInstance.get("/api/market/"+id).then((data)=>{
        console.log("market",data.data);
        setMarket(data.data);
      }).catch((err)=>{
        console.log(err);
      })
    }

    const getAddresses = (id)=>{
      axiosInstance.get("/api/user/address/"+id).then((data)=>{
        setAddresses(data.data);
        data.data && setAddress(data.data[0])
      }).catch((err)=>{
        console.log(err);
      })
    }

    const MakeOrder = ()=>{
      profile &&  axiosInstance.post("/api/order/create",{
        sum:jemi,
        is_cash:toleg,
        delivery_time_status:time,
        MarketId:market.id,
        UserId:profile.id,
        AddressId:address.id,
        delivered:false,
        orderedProducts:orderedProduct,
        cashBackMoney:profile?jemi*market?.cashBackPrasent/100:null,
        cashBack:(profile ? market.cashBack:false)
      }).then((data)=>{
        console.log(data.data);
        message.success("sargyt edildi")
        let pro = JSON.parse(localStorage.getItem("SargytProducts"));
        RemoveAllPro(market.id)
        localStorage.removeItem("SargytProducts");
        history.push({
          pathname:"/success"
        });
      }).catch((err)=>{
        message.error("order error")
        console.log("error make order",err);
      });


      !(rec_address && rec_name && rec_number) && !profile && message.warn("maglumatlaryňyzy doly giriziň!");
      !profile && rec_address && rec_name && rec_number && axiosInstance.post("/api/user/address/rec_create",{
        rec_address,
        rec_name,
        rec_number
      }).then((data)=>{
        setrec_address("");setrec_name("");setrec_number("")
         if(data.data.msg === "Suссessfully"){
          data.data && data.data.data.id && axiosInstance.post("/api/order/create",{
                        sum:jemi,
                        is_cash:toleg,
                        delivery_time_status:time,
                        MarketId:market.id,
                        UserId:null,
                        AddressId:data.data.data.id,
                        delivered:false,
                        orderedProducts:orderedProduct
                      }).then((data)=>{
                        console.log(data.data);
                        let pro = JSON.parse(localStorage.getItem("SargytProducts"));
                        RemoveAllPro(market.id)
                        localStorage.removeItem("SargytProducts");
                        history.push({
                          pathname:"/success"
                        });
                      }).catch((err)=>{
                        console.log(err);
                      })
         }else{
           message.warn("maglumatlaryňyzy doly giriziň!")
         }
      }).catch((err)=>{
        console.log(err);
      });
    }

    console.log(market?.dastawkaEndI?.slice(0,2))
    return(
    <main class="main">
        <div class="main-second-box main-second-box__big">
          <section class="payment">
            <section class="payment__bred-crumb bred-crumb">
              <div class="bred-crumb__container __container">
                <div class="bred-crumb__links">
                  <Link to="/home" class="bred-crumb__link">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="15"
                      height="15"
                      viewBox="0 0 15 15"
                    >
                      <path
                        id="noun_Home_2102808"
                        d="M7.028,12.906l.327-.276v7.745A.625.625,0,0,0,7.98,21H19.02a.625.625,0,0,0,.625-.625V12.63l.327.276a.625.625,0,0,0,.806-.955l-3-2.533V7.039a.625.625,0,1,0-1.25,0V8.363L13.9,6.147a.625.625,0,0,0-.806,0l-6.875,5.8a.625.625,0,1,0,.806.955ZM12.1,19.75V15.238H14.9V19.75ZM13.5,7.443l4.895,4.132V19.75H16.147V14.613a.625.625,0,0,0-.625-.625H11.478a.625.625,0,0,0-.625.625V19.75H8.605V11.575Z"
                        transform="translate(-6 -6)"
                      />
                    </svg>
                  </Link>
                  <Link to={`/market/${MarketId}`}  class="bred-crumb__link"
                    >{dil==="TM"?"Çynar market":(dil==="RU"?"Чынар маркет":"Cynar market")}
                  </Link>
                  <Link
                    to="/basket"
                     class="bred-crumb__link " //bred-crumb__link--disabled
                    > {dil==="TM"?"Sebet":(dil==="RU"?"Корзина":"Basket")} </Link >
                  <Link to="/toleg" class="bred-crumb__link"
                    >{dil==="TM"?"Töleg görnüşi":(dil==="RU"?"Способ оплаты":"Payment type")}</Link>
                </div>
              </div>
            </section>

            <div class="payment__container __container">
                <section class="payment__row">
                    <div class="payment__box-1">
                    <div class="payment__basket-mini basket-mini">
                        <h3 class="basket-mini__big-title">{dil==="TM"?"Siziň sebediňiz":(dil==="RU"?"Ваша корзина":"Your basket")}</h3>
                        <section class="basket-mini__row">
                           {products.map((product)=>{
                              return <TolegCart product={product.product}/>
                           }) }
                        </section>
                    </div>
                    </div>

                    <div class="payment__box-2">
                        <div class="payment__checkbox-row checkbox-row">
                            <div class="checkbox-row__title">{dil==="TM"?"Töleg görnüşi":(dil==="RU"?"Способ оплаты":"Payment type")}</div>
                            <div class="checkbox-row__container">
                            <label class="container">
                                <input type="radio" name="payment-radio" onClick={()=>setToleg(true)} checked={toleg}/>
                                <p>{dil==="TM"?"Nagt töleg":(dil==="RU"?"Наличный оплаты":"Cash payment")}</p>
                                <span></span>
                            </label>
                            {market.is_cart &&<label class="container">
                                <input type="radio" name="payment-radio" onClick={()=>setToleg(false)} checked={!toleg} />
                                <p>{dil==="TM"?"Kart üsti töleg":(dil==="RU"?"Оплата картой":"Card payment")}</p>
                                <span></span>
                            </label>}
                            </div>
                            
                            <div style={{marginBottom:"10px",marginTop:"15px"}} class="checkbox-row__title">{dil==="TM"?"Eltip bermeli wagty":(dil==="RU"?"Время доставки":"Time delivery")}</div>
                            <div class="checkbox-row__container">
                            <label class="container">
                                <input type="radio" name="address-radio" onClick={()=>setTime(1)} checked={time===1?true:false} disabled={!(new Date().getHours()<+market?.dastawkaEndI?.slice(0,2))}/>
                                <p>{market.dastawkaStartI}-{market.dastawkaEndI} ( {dil==="TM"?"Şu gün":(dil==="RU"?"Сегодня":"Today")} )</p>
                                <span></span>
                            </label>
                            <label class="container">
                                <input type="radio" name="address-radio" onClick={()=>setTime(2)} checked={time===2?true:false} disabled={!(new Date().getHours()< +market?.dastawkaEndII?.slice(0,2))}/>
                                <p>{market.dastawkaStartII}-{market.dastawkaEndII} ( {dil==="TM"?"Şu gün":(dil==="RU"?"Сегодня":"Today")} )</p>
                                <span></span>
                            </label>
                            <label class="container">
                                <input type="radio" name="address-radio" onClick={()=>setTime(3)} checked={time===3?true:false} />
                                <p>{market.dastawkaStartI}-{market.dastawkaEndI} ( {dil==="TM"?"Ertir":(dil==="RU"?"Завтра":"Tomorow")} )</p>
                                <span></span>
                            </label>
                            <label class="container">
                                <input type="radio" name="address-radio" onClick={()=>setTime(4)} checked={time===4?true:false} />
                                <p>{market.dastawkaStartII}-{market.dastawkaEndII} ( {dil==="TM"?"Ertir":(dil==="RU"?"Завтра":"Tomorow")} )</p>
                                <span></span>
                            </label>
                            </div>
                        </div>
                        <div class="basket-mini__price-box">
                        {market?.cashBack&&<div class="checkbox-row__title">{dil==="TM"?`Marketde ${market?.cashBackPrasent}% cashBack ulgamy isleyar.`:(dil==="RU"?`На маркете  действует система ${market?.cashBackPrasent}% cashBack.`:`There is a ${market?.cashBackPrasent}% cashBack system on the market`)}</div>}
                           
                            <div class="basket-mini__price-box-container">
                            <h3>{dil==="TM"?"Eltip bermek":(dil==="RU"?"Доставлять":"To deliver")}</h3>
                            <p>{market.dastawkaPrice} TMT</p>
                            </div>
                            {market?.cashBack&&profile&&<div class="basket-mini__price-box-container">
                            <h3>Cashback</h3>
                            <p>{jemi*market?.cashBackPrasent/100}  TMT</p>
                            </div>}
                            <div class="basket-mini__price-box-container">
                            <h3>{dil==="TM"?"Jemi":(dil==="RU"?"Сумма":"Sum")}</h3>
                            <p>{jemi}  TMT</p>
                            </div>
                        </div>
                    </div>
                   {!profile && <div class="payment__box-3">
                        <div class="payment__btns">
                            <a href="/login">{dil==="TM"?"Giriş":(dil==="RU"?"Войти":"Login")}</a>
                            <a href="/sign-up">{dil==="TM"?"Registrasiýa":(dil==="RU"?"Регистрация":"Registration")}</a>
                            <button class="payment__btn-big">
                            {dil==="TM"?"Registrasiýasyz sargamak":(dil==="RU"?"Сделать заказ без регистрации":"Make an order without registration")}
                            </button>
                        </div>
                        <div class="profile__box">
                            <div class="profile__title">{dil==="TM"?"Kontakt maglumatlary":(dil==="RU"?"Контактная информация":"Contact details")}</div>
                            <div class="profile__input">
                            <input value={rec_name} onChange={(e)=>setrec_name(e.target.value)} type="text" placeholder={dil==="TM"?"Adyňyz":(dil==="RU"?"Имя":"Name")} />
                            </div>
                            <div class="profile__input">
                            <input value={rec_address} onChange={(e)=>setrec_address(e.target.value)} type="text" placeholder={dil==="TM"?"Salgyňyz":(dil==="RU"?"Ваш адрес":"Your address")} />
                            </div>
                            <div class="profile__input">
                            <input  value={rec_number} onChange={(e)=>setrec_number(e.target.value)} type="phone" placeholder={dil==="TM"?"Telefon belgiňiz":(dil==="RU"?"Ваш номер телефона":"Your phone number")} />
                            </div>
                        </div>
                    </div>}
                </section>
              {profile && <div class="payment__send-adress">
                <div class="payment__send-adress-titles">
                  <h2>{dil==="TM"?"Eltip berme salgysy":(dil==="RU"?"Адрес доставки":"Delivery address")}</h2>
                  <h3 id="payment-adress">{address.rec_address}</h3>
                </div>
                <button onClick={()=>setPopUp(!popUp)}  class="payment__send-adress-send -js-popup">
                  {dil==="TM"?"Üýtgetmek":(dil==="RU"?"Изменить":"Edit")}
                </button>
              </div>}
              <div class="payment__send-btn">
                <button onClick={()=>MakeOrder()}>{dil==="TM"?"Töleg":(dil==="RU"?"Оплата":"Payment")}</button>
              </div>
            </div>
          </section>
          <section onClick={()=>setPopUp(!popUp)} className={`payment-popup -js-popup ${active1}`}>
            <div  class="payment-popup__container __container ">
              <h2 class="payment-popup__title">
                {dil==="TM"?"Eltip bermeli salgyňyzy saýlaň":(dil=="RU"?"Выберите адрес, который вы хотите доставить":"Select the address you want to deliver")}
              </h2>
              <div class="payment-popup__row">
                  {addresses && addresses.map((address)=>{
                    return <TolegAddress onclick={()=>AddressHandler(address)} address={address}/>
                  })}
              </div>
            </div>
          </section>
        </div>
      </main>
    )
}

export default Toleg;