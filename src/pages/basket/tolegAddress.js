import React, { useContext } from "react";

import "../../css/style.css";

import home from "../../img/icons/home.svg";
import user from "../../img/icons/user.svg";
import phoneNumber from "../../img/icons/phone-number.svg"
import { SebedimContext } from "../../context/Sebedim";

const TolegAddress = (props)=>{

  const {dil} = useContext(SebedimContext)
    return(
        <button onClick={()=>props.onclick()} class="payment-popup__column -js-adress-update">
                  <h2>
                    <div class="payment-popup__icon">
                      <img src={home} alt="home" />
                    </div>
                    <p class="adress">{props.address.rec_address}</p>
                  </h2>
                  <h2>
                    <div class="payment-popup__icon">
                      <img src={user} alt="home" />
                    </div>
                    <p>{props.address.rec_name}</p>
                  </h2>
                  <h2>
                    <div class="payment-popup__icon">
                      <img src={phoneNumber} alt="home" />
                    </div>
                    <p>+{props.address.rec_number}</p>
                  </h2>
                </button>
    )
}

export default TolegAddress;