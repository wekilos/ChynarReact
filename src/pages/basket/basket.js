import React, { useContext, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import BasketCart from "../../components/basketCart";
import { message } from "antd";

import "../../css/style.css";
import { axiosInstance } from "../../utils/axiosIntance";
import { useState } from "react";
import { SebedimContext } from "../../context/Sebedim";

const Basket = (props)=> {

  const history = useHistory();
  const { dil } = useContext(SebedimContext);
  const [markets,setMarkets] = useState([]);
  const [incBool,setIncBool] = useState(false)
  const marketId = localStorage.getItem("MarketId");
  useEffect(()=>{
    let welayatId = localStorage.getItem("welayatId");
    getMarkets(welayatId);
  },[incBool])


    const SargytEt = (MarketId,jemi,products)=>{
            // message.success(MarketId);
            history.push({
              pathname:`/toleg/${MarketId}/${jemi}`
            });
            localStorage.setItem("SargytProducts",JSON.stringify(products));
    }

    const getMarkets = (id)=>{
      axiosInstance.get("/api/kategoryOfMarkets/"+id).then((data)=>{
        let massiw = []
        data.data.map((kategory)=>{
          kategory.Markets.map((market)=>{
            massiw.push({
              id:market.id,
              name_tm:market.name_tm,
              name_ru:market.name_ru,
              name_en:market.name_en});
          })
        });
        getProducts(massiw)
      }).catch((err)=>{
        console.log(err);
      })
    }

    const getProducts=(massiw)=>{
        let products = JSON.parse(localStorage.getItem("ChynarSebedim"));
        let marketWithPro = [];
        let marketPro = [];
        let jemi = 0;
        massiw.map((market)=>{
          products.map((product)=>{
            if(product.MarketId===market.id){
                marketPro.push(product);
                jemi = jemi+(product.sany * (product.product.is_valyuta_price?
                  (product.product.is_sale?
                    product.product.sale_price*product.product.Config.currency_exchange:
                    product.product.price*product.product.Config.currency_exchange):
                  (product.product.is_sale?product.product.sale_price:product.product.price) ) )
            }
          })
          marketWithPro.push({market:market,products:marketPro,jemi:jemi});
          marketPro = [];
          jemi = 0;
        });
        setMarkets(marketWithPro);
    }


    return(
        <main class="main">
        <div class="main-second-box main-second-box__big">
          <div class="bred-crumb__container __container">
            <div class="bred-crumb__links">
              <Link to="/home" class="bred-crumb__link">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="15"
                  height="15"
                  viewBox="0 0 15 15"
                >
                  <path
                    id="noun_Home_2102808"
                    d="M7.028,12.906l.327-.276v7.745A.625.625,0,0,0,7.98,21H19.02a.625.625,0,0,0,.625-.625V12.63l.327.276a.625.625,0,0,0,.806-.955l-3-2.533V7.039a.625.625,0,1,0-1.25,0V8.363L13.9,6.147a.625.625,0,0,0-.806,0l-6.875,5.8a.625.625,0,1,0,.806.955ZM12.1,19.75V15.238H14.9V19.75ZM13.5,7.443l4.895,4.132V19.75H16.147V14.613a.625.625,0,0,0-.625-.625H11.478a.625.625,0,0,0-.625.625V19.75H8.605V11.575Z"
                    transform="translate(-6 -6)"
                  />
                </svg>
              </Link>
              <Link to={`/market/${marketId}`} class="bred-crumb__link">{dil==="TM"?"Çynar market":(dil==="RU"?"Чынар маркет":"Cynar market")} </Link>
              
              <Link to="/basket" class="bred-crumb__link">{dil==="TM"?"Sebet":(dil==="RU"?"Корзина":"Basket")} </Link>
            </div>
          </div>
          <section class="basket">
            <div class="basket__container __container">
              
              {
                markets && markets.map((market)=>{
                  return market.products[0] && <React.Fragment>
                      <table class="basket__table">
                          <caption style={{width:"320px",fontSize:"20px"}}>{dil==="TM"?market.market.name_tm:(dil==="RU"?market.market.name_ru:market.market.name_en)}</caption>
                          {
                            market.products && market.products.map((product)=>{
                              return <BasketCart bool={[incBool,setIncBool]} product = {product} />
                            })
                          }
                        </table>
                        <div class="basket__counts">
                          <p><span>{dil==="TM"?"Jemi":(dil==="RU"?"Сумма":"Sum")}</span> <span>{market.jemi} {dil==="TM"?"TMT":(dil==="RU"?"ТМТ":"ТМТ")}</span></p>
                        </div>
                        <div class="basket__send">
                          <div onClick={()=>SargytEt(market.market.id,market.jemi,market.products)}>
                              <input type="submit" value={dil==="TM"?"Sargyt et":(dil==="RU"?"Сделать заказ":"Make Order")} />
                          </div>
                        </div>
                  </React.Fragment>
                })
              }

            </div>
          </section>
        </div>
      </main>
    )
}

export default Basket;