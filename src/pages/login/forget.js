import { message } from "antd";
import { useHistory } from "react-router-dom";
import axios from "axios";
import React, {useContext, useState} from "react";
import { Link } from "react-router-dom";
// import { AutoTabProvider } from 'react-auto-tab'

import none_hide from "../../img/icons/noun_hide.svg";
import none_visible from "../../img/icons/noun_visible.svg"

import "../../css/style.css";
import { axiosInstance } from "../../utils/axiosIntance";
import { SebedimContext } from "../../context/Sebedim";

const Forget = ()=>{

  const {dil } = useContext(SebedimContext);
    const history = useHistory()
    const [number,setNumber] = useState(9936);
    const [codeBool,setCodeBool] = useState(false);
    const [passwordBool,setPasswordBool] = useState(false);
    const [code,setCode] = useState("");
    const [code1,setCode1] = useState("");
    const [code2,setCode2] = useState("");
    const [code3,setCode3] = useState("");
    const [code4,setCode4] = useState("");
    const [data,setData] = useState({});
    const [auto,setAuto] = useState(1);
    const [newPassword,setNewPassword] = useState();

    const SendNumber = ()=>{
      (number.length<11) && message.warn(dil==="TM"?"Telefon belgini doly girizin!":(dil==="RU"?"Введите свой номер телефона полностью!":"Enter your phone number completely!"))
    !(number.length<11) &&  axiosInstance.post("/api/send",{
        number:number
      }).then((data)=>{
        if(data.data.code){
          console.log(data.data)
            setCodeBool(true);
            setData(data.data);
        }
      }).catch((err)=>{
        console.log(err);
      })
    }

    const CheckCode = ()=>{
      console.log(data,code1)
      if(data.code == code1){
        setPasswordBool(true);
        setCodeBool(false);
      }else{
        message.warn(dil==="TM"?"Siziň kodyňyz ýalňyş!":(dil==="RU"?"Ваш код неверен!":"Your code is incorrect!"))
      }
    }
    const ChangePassword = ()=>{
      console.log(newPassword,number)
      axiosInstance.post("/api/user/forget",{
            phoneNumber:number,
            password:newPassword,
      }).then((data)=>{
          console.log(data.data);
          if(data.data.msg==="Suссessfully"){
          localStorage.setItem("ChynarProfile",JSON.stringify(data.data))
          message.success(dil==="TM"?"Açar sözüňiz üstünlikli täzelendi!":(dil==="RU"?"Ваш пароль был успешно обновлен!":"Your password has been successfully updated!"));
          history.push({
            pathname:"/profile"
          })
          }else{
            message.warn(dil==="TM"?"Internediňizi barlaň!":(dil==="RU"?"Проверьте свой интернет!":"Check your internet!"))
          }
      }).catch((err)=>{
        console.log(err);
      })
    }
    

    

    return (
      <React.Fragment>
        {!codeBool && !passwordBool && <main class="main">
        <section class="input-form">
          <div class="input-form__container __container">
            <section class="input-form__form">
              <div class="input-form__title">{dil==="TM"?"Açarsözüni täzelemek":(dil==="RU"?"Обновление пароля":"Password update")}</div>

              <div class="input-form__input--form">
                <div class="input-form__input">
                  <input onChange={(e)=>setNumber(e.target.value)} value={number} type="number" placeholder={dil==="TM"?"Telefon":(dil==="RU"?"Телефон":"Phone")} />
                </div>
              </div>
              <div class="input-form__forgot-pass"></div>
              <div class="input-form__button">
                <input onClick={()=>SendNumber()} value={dil==="TM"?"Gir":(dil==="RU"?"Войти":"Login")} />
              </div>
              <div class="input-form__sign-up">
                <p>{dil==="TM"?"Entek agzalygyň ýokmy?":(dil==="RU"?"Еще не зарегистрированы?":"Not a member yet?")}</p>
                <Link to="/sign-up">{dil==="TM"?"Agza bol":(dil==="RU"?"Зарегистрироваться":"Sign up")}</Link>
              </div>
            </section>
          </div>
        </section>
      </main>}
      {codeBool && !passwordBool && <main class="main">
        <section class="input-form">
          <div class="input-form__container __container">
            <div class="input-form__title">
              {dil==="TM"?"Biz size 4 sandan ybarat bolan açarsözüni ugratdyk":(dil==="RU"?"Мы отправили вам 4-значный пароль":"We have sent you a 4-digit password")}
            </div>
            <div class="input-form__title-2">
              {dil==="TM"?"Haýyşt":(dil==="RU"?"Пожалуйста":"Please")} +993 6*****{!number.length<13 && number?.slice(9)} {dil==="TM"?"telefon belgiňizi barlaň":(dil==="RU"?"проверьте свой номер телефона":"check your phone number")}
            </div>
            <section class="input-form__form">
             { <div class="input-form__reg-inputs">
              
                  <input
                    type="text"
                    placeholder="&#9913;&#9913;&#9913;&#9913;"
                    // class="reg-inputs"
                    style={{width:"320px"}}
                    maxlength="4"
                    size="4"
                    autoFocus={auto===1 && true}
                    onChange={(e)=>{setCode1(e.target.value);}}
                  />
                

              </div>}
              
              <div class="input-form__forgot-pass"></div>
              <div class="input-form__button">
                <input onClick={()=>CheckCode()} type="submit" value={dil==="TM"?"Tassykla":(dil==="RU"?"Подтверждать":"Confirm")} disabled={!(code1.length===4)} />
              </div>
              <div class="input-form__sign-up">
                <p>{dil==="TM"?"Entek agzalygyň ýokmy?":(dil==="RU"?"Еще не зарегистрированы?":"Not a member yet?")}</p>
                <a href="/sign-up">{dil==="TM"?"Agza bol":(dil==="RU"?"Зарегистрироваться":"Sign up")}</a>
              </div>
            </section>
          </div>
        </section>
      </main>}
      {!codeBool && passwordBool && <main class="main">
        <section class="input-form">
          <div class="input-form__container __container">
            <section class="input-form__form">
              <div class="input-form__title">{dil==="TM"?"Açarsözüni täzelemek":(dil==="RU"?"Обновление пароля":"Password update")}</div>

              <div class="input-form__input--form">
                <div class="input-form__input">
                  <input onChange={(e)=>setNewPassword(e.target.value)} value={newPassword} type="text" placeholder={dil==="TM"?"Täze açar sözi":(dil==="RU"?"Новое ключевое слово":"New password")} />
                </div>
              </div>
              <div class="input-form__forgot-pass"></div>
              <div class="input-form__button">
                <input onClick={()=>ChangePassword()} type="submit" value={dil==="TM"?"Täzele":(dil==="RU"?"Обновлять":"Update")} />
              </div>
              {/* <div class="input-form__sign-up">
                <p>Entek agzalygyň ýokmy?</p>
                <Link to="/sign-up">Agza bol</Link>
              </div> */}
            </section>
          </div>
        </section>
      </main>}
      </React.Fragment>
    )
}

export default Forget;