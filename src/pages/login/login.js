import { message } from "antd";
import React, { useState, useContext } from "react";
import { Link,useHistory } from "react-router-dom";
import { SebedimContext } from "../../context/Sebedim";

import "../../css/style.css";

import none_hide from "../../img/icons/noun_hide.svg";
import none_visible from "../../img/icons/noun_visible.svg"
import { axiosInstance } from "../../utils/axiosIntance";

const Login = ()=>{

  const {dil} = useContext(SebedimContext);
  const history = useHistory();
    const [visible,setVisible] = useState(false);
    const [number,setNumber] = useState(9936);
    const [password,setPassword] = useState();

    const login = ()=>{
        axiosInstance.post("/api/user/login",{
            phoneNumber:number,
            password:password
        }).then((data)=>{
            console.log(data.data);
            if(data.data.login===false && data.data.msg!=="Your username or password is invalid!"){
                message.warn(dil==="TM"?"Hasaba alynmadyk ulanyjy!":(dil==="RU"?"Незарегистрированный пользователь!":"Unregistered user!"))
            }
            if(data.data.login===false && data.data.msg==="Your username or password is invalid!"){
                message.warn(dil==="TM"?"Nomuryňyz ýa-da açar sözüňiz ýalňyş!":(dil==="RU"?"Ваш номер или пароль неверны!":"Your number or password is incorrect!"))
            }
            if(data.data.login===true){
                message.success(dil==="TM"?"Üstünlikli girdiňiz!":(dil==="RU"?"Удачи!":"Successfully!"));
                localStorage.setItem("ChynarProfile",JSON.stringify(data.data));
                history.push({
                  pathname:"/profile"
                });
            }
        }).catch((err)=>{
            console.log(err);
        })
    }
    return(
        <main class="main">
        <section class="input-form">
          <div class="input-form__container __container">
            <div class="input-form__form">
              <div class="input-form__title">{dil==="TM"?"Giriş":(dil==="RU"?"Войти":"Login")}</div>
              <div class="input-form__input--form">
                <div class="input-form__input">
                  <input onChange={(e)=>setNumber(e.target.value)} value={number} type="number" placeholder={dil==="TM"?"Telefon belgiňiz":(dil==="RU"?"Ваш номер телефона":"Your phone number")} />
                </div>
                <div class="input-form__input input-form__input--password">
                  <input
                    onChange={(e)=>setPassword(e.target.value)} value={password}
                    type={!visible && "password"}
                    placeholder={dil==="TM"?"Açarsöziňiz":(dil==="RU"?"Ваш пароль":"Your password")}
                    class="p-password"
                  />
                  
                  {!visible && <button onClick={()=>setVisible(true)} id="p-hidden">
                    <img src={none_hide} />
                  </button>}
                  {visible && <button onClick={()=>setVisible(false)} id="p-hidden">
                    <img src={none_visible} />
                  </button>}
                </div>
              </div>
              <div class="input-form__forgot-pass">
                <Link to="/forget">{dil==="TM"?"Açarsözümi ýatdan çykardym":(dil==="RU"?"Забыл пароль":"Forget password")}</Link>
              </div>

              <div class="input-form__button">
                <input onClick={()=>login()} type="submit" value={dil==="TM"?"Gir":(dil==="RU"?"Войти":"Login")} />
              </div>
              <div class="input-form__sign-up">
                <p>{dil==="TM"?"Entek agzalygyň ýokmy?":(dil==="RU"?"Еще не зарегистрированы?":"Not a member yet?")}</p>
                <Link to="/sign-up">{dil==="TM"?"Agza bol":(dil==="RU"?"Зарегистрироваться":"Sign up")}</Link>
              </div>
            </div>
          </div>
        </section>
      </main>
    )
}

export default Login;