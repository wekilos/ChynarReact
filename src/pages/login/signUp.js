import { message } from "antd";
import React, { useState, useContext } from "react"
import { Link, useHistory } from "react-router-dom";
import { SebedimContext } from "../../context/Sebedim";

import "../../css/style.css";

import none_hide from "../../img/icons/noun_hide.svg";
import none_visible from "../../img/icons/noun_visible.svg"
import { axiosInstance } from "../../utils/axiosIntance";

const SignUp = ()=>{

  const {dil} = useContext(SebedimContext);
  const history = useHistory();
    const [visible,setVisible] = useState(false);
    const [user,setUser] = useState({phone:9936});
    const [codeBool,setCodeBool] = useState(false);
    const [data,setData] = useState();
    const [code1,setCode1] = useState("");

    const create = ()=>{
        axiosInstance.post("/api/user/create",{
            fname:user.name, 
            lastname:user.surname,
            phoneNumber:user.phone,
            password:user.password,
            UserTypeId:1,
            rec_name:user.name,
            rec_address:user.address,
            rec_number:user.phone
        }).then((data)=>{
            console.log(data.data);
            if(data.data.UserTypeId===1){
                message.success(dil==="TM"?"Siz ustunlikli ulgama girdiňiz!":(dil==="RU"?"Вы вошли в систему!":"You are logged in!"));
                localStorage.setItem("ChynarProfile",JSON.stringify(data.data))
                history.push({
                  pathname:"/profile"
                })
            }else{
                message.warn(dil==="TM"?"Bu nomur bilen ulgama agza bolunan!":(dil==="RU"?"Членство в этой системе с этим номером!":"Membership in this system with this number!"))
            }
        }).catch((err)=>{
            console.log(err);
        })
    }

    const CheckCode = ()=>{
        if(data.code==code1){
          create()
        }else{
          message.warn(dil==="TM"?"Siziň kodyňyz ýalňyş!":(dil==="RU"?"Ваш код неверен!":"Your code is incorrect!"))
        }
    }
  
    const SendNumber = (e)=>{
      console.log(user.phone)
      axiosInstance.post("/api/send",{
          number:user.phone
      }).then((data)=>{
        console.log(data.data);
        if(data.data.code){
          setData(data.data)
          setCodeBool(true);
        }
      }).catch((err)=>{
        console.log(err);

      })
    }
    return(
        <main class="main">
       {!codeBool && <section class="input-form">
          <div class="input-form__container __container">
            <section class="input-form__form">
              <div class="input-form__title">{dil==="TM"?"Registrasiýa":(dil==="RU"?"Регистрация":"Sign-up")}</div>
              <div class="input-form__input--form">
                <div class="input-form__input">
                  <input onChange={(e)=>setUser({...user,name:e.target.value})} value={user.name} type="text" placeholder={dil==="TM"?"Adyňyz":(dil==="RU"?"Ваше имя":"Your name")} />
                </div>
                <div class="input-form__input">
                  <input onChange={(e)=>setUser({...user,surname:e.target.value})} value={user.surname} type="text" placeholder={dil==="TM"?"Familýaňyz":(dil==="RU"?"Ваша фамилия":"Your surname")} />
                </div>
                <div class="input-form__input">
                  <input onChange={(e)=>setUser({...user,phone:e.target.value})} value={user.phone} type="tel" placeholder={dil==="TM"?"Telefon belgiňiz":(dil==="RU"?"Ваш номер телефона":"Your phone number")} />
                </div>

                <div class="input-form__input">
                  <input onChange={(e)=>setUser({...user,address:e.target.value})} value={user.address} type="text" placeholder={dil==="TM"?"Salgyňyz":(dil==="RU"?"Ваш адрес":"Your address")} />
                </div>
                {/* <div class="input-form__input">
                  <input type="password" placeholder="Password" />
                </div> */}
                 
                <div class="input-form__input input-form__input--password">
                  <input
                    onChange={(e)=>setUser({...user,password:e.target.value})} value={user.password} 
                    type={!visible && "password"}
                    placeholder={dil==="TM"?"Açarsöziňiz":(dil==="RU"?"Ваш пароль":"Password")}
                    class="p-password"
                  />
                  
                  {!visible && <button onClick={()=>setVisible(true)} id="p-hidden">
                    <img src={none_hide} />
                  </button>}
                  {visible && <button onClick={()=>setVisible(false)} id="p-hidden">
                    <img src={none_visible} />
                  </button>}
                </div>
                {/* <div class="input-form__checkbox">
                  <input type="checkbox" id="i-agree" />
                  <label for="i-agree">
                    <span></span>
                    <p>I agree to the Google Terms of Service and Privacy Policy</p>
                  </label>
                 </div>  */}
              </div>
              {/* <div class="input-form__forgot-pass"></div> */}
              <div onClick={()=>SendNumber()} class="input-form__button">
                <input  type="submit" value={dil==="TM"?"Registrasiýa":(dil==="RU"?"Регистрация":"Sign-up")} />
              </div>
              <div class="input-form__sign-up">
                <p>{dil==="TM"?"Akkauntyňyz barmy?":(dil==="RU"?"У тебя есть аккаунт?":"Do you have an account?")}</p>
                <Link to="/login">{dil==="TM"?"Gir":(dil==="RU"?"Войти":"Login")}</Link>
              </div>
            </section>
          </div>
        </section>}
        {codeBool && <React.Fragment>
              <main class="main">
        <section class="input-form">
          <div class="input-form__container __container">
            <div class="input-form__title">
            {dil==="TM"?"Biz size 4 sandan ybarat bolan açarsözüni ugratdyk":(dil==="RU"?"Мы отправили вам 4-значный пароль":"We have sent you a 4-digit password")}
            </div>
            <div class="input-form__title-2">
            {dil==="TM"?"Haýyşt":(dil==="RU"?"Пожалуйста":"Please")} +993 6*****{user.phone && user.phone.slice(9)} {dil==="TM"?"telefon belgiňizi barlaň":(dil==="RU"?"проверьте свой номер телефона":"check your phone number")}
            </div>
            <section class="input-form__form">
             { <div class="input-form__reg-inputs">
              
                  <input
                    type="text"
                    placeholder="&#9913;&#9913;&#9913;&#9913;"
                    // class="reg-inputs"
                    style={{width:"320px"}}
                    maxlength="4"
                    size="4"
                    autoFocus={ true}
                    onChange={(e)=>{setCode1(e.target.value);}}
                  />
                

              </div>}
              
              <div class="input-form__forgot-pass"></div>
              <div class="input-form__button">
                <input onClick={()=>CheckCode()} type="submit" value={dil==="TM"?"Tassykla":(dil==="RU"?"Подтверждать":"Confirm")} disabled={!(code1.length===4)} />
              </div>
              <div class="input-form__sign-up">
              <p>{dil==="TM"?"Entek agzalygyň ýokmy?":(dil==="RU"?"Еще не зарегистрированы?":"Not a member yet?")}</p>
              <a href="/sign-up">{dil==="TM"?"Agza bol":(dil==="RU"?"Зарегистрироваться":"Sign up")}</a>
              </div>
            </section>
          </div>
        </section>
      </main>

          </React.Fragment>
        }
      </main>
    )
}

export default SignUp;