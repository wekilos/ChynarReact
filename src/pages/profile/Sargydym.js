import React, {useContext} from "react";
import { SebedimContext } from "../../context/Sebedim";

import "../../css/style.css";

const Sargydym = (props)=>{

  const { dil } = useContext(SebedimContext);
    return (
        <div class="profile__shopping-bag shopping-bag">
                <section class="shopping-bag__row">
                  <article
                    class="shopping-bag__column shopping-bag__column_big"
                  >
                    <div class="shopping-bag__menu">
                      <div
                        class="
                          shopping-bag__status shopping-bag__status_success
                        "
                      >
                        {props?.order?.Status ? (dil==="TM"?props?.order?.Status?.name_tm:(dil==="RU"?props.order.Status?.name_ru:props.order.Status?.name_en)) :(dil==="TM"?"Garaşylýar!":(dil==="RU"?"Ожидал!":"Expected!"))}
                      </div>
                      <div class="shopping-bag__date">{props.order.order_date_time.slice(0,4)}.{props.order.order_date_time.slice(5,7)}.{props.order.order_date_time.slice(8,10)}</div>
                    </div>
                    <div class="shopping-bag__table">
                      <table>
                        <tbody>
                          <tr>
                            <td>{dil==="TM"?"Sargyt":(dil==="RU"?"Заказ":"Order")} №</td>
                            <td>#{props.order.id}</td>
                          </tr>
                          <tr>
                            <td>{dil==="TM"?"Satyn alyjy":(dil==="RU"?"Покупатель":"Buyer")}</td>
                            <td>{props.order.User.fname} {props.order.User.lastname}</td>
                          </tr>
                          <tr>
                            <td>{dil==="TM"?"Tel":(dil==="RU"?"Тел":"Tel")}</td>
                            <td>+{props.order.User.phoneNumber}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </article>
                  <article
                    class="shopping-bag__column shopping-bag__column_big"
                  >
                    <div
                      class="shopping-bag__table shopping-bag__table_product"
                    >
                      <table>
                        <tbody>
                          <tr>
                            <th>{dil==="TM"?"Harydyň ady":(dil==="RU"?"Название продукта":"The name of the product")}</th>
                            <th>{dil==="TM"?"Model":(dil==="RU"?"Модель":"Model")} №</th>
                            <th>{dil==="TM"?"Mukdary":(dil==="RU"?"Количество":"Amount")}</th>
                            <th>{dil==="TM"?"Bahasy":(dil==="RU"?"Цена":"Price")}</th>
                          </tr>
                          {props.order.OrderedProducts.map((product)=>{
                              return <tr>
                              <td><a href="">{dil==="TM"?product.Product.name_tm:(dil==="RU"?product.Product.name_ru:product.Product.name_en)}</a></td>
                              <td>{product.Product.id}</td>
                              <td>{product.amount} {dil==="TM"?product.Product.Unit.name_tm:(dil==="RU"?product.Product.Unit.name_ru:product.Product.Unit.name_en)}</td>
                              <td class="shopping-bag-table-price">{product.Product.is_valyuta_price?product.Product.price*product.Product.Config.currency_exchange:product.Product.price} TMT</td>
                            </tr>
                          }) }
                          
                        </tbody>
                      </table>
                    </div>
                  </article>
                  <article
                    class="shopping-bag__column shopping-bag__column_big"
                  >
                    <div class="shopping-bag__table">
                      <table>
                        <tbody>
                          {props?.order?.cashBack && <tr>
                            <td>CashBack</td>
                            <td>{props?.order?.cashBackMoney} TMT</td>
                          </tr>}
                          <tr>
                            <td>{dil==="TM"?"Jemi":(dil==="RU"?"Сумма":"Sum")}</td>
                            <td>{props.order.sum} TMT</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </article>
                </section>
              </div>
    )
}

export default Sargydym;