import { OmitProps } from "antd/lib/transfer/ListBody";
import React, { useState, useContext } from "react";
import { Link } from "react-router-dom";
import { SebedimContext } from "../../context/Sebedim";

import "../../css/style.css";

import eye from "../../img/icons/eye-white.svg";

const Sargytlarym = (props)=>{

  const { dil } = useContext(SebedimContext);
    const [garashylyar,setGarashylyar] = useState("shopping-bag__status_waiting");
    const [gowshuryldy,setGowshuryldy] = useState("shopping-bag__status_success");
    const [yatyryldy,setYatyryldy] = useState("shopping-bag__status_stoped");

  

    return(
        
                  <article class="shopping-bag__column">
                    <div class="shopping-bag__menu">
                      <div
                        class="
                          shopping-bag__status shopping-bag__status_success
                        "
                      >
                        {props?.order?.Status ? (dil==="TM"?props?.order?.Status?.name_tm:(dil==="RU"?props.order.Status?.name_ru:props.order.Status?.name_en)) :(dil==="TM"?"Garaşylýar!":(dil==="RU"?"Ожидал!":"Expected!"))}
                      </div>
                      <div class="shopping-bag__date">{props.order.order_date_time.slice(0,10)}</div>
                      <Link onClick={()=>props.ShowSargydym(props.order)}
                        // to="/sargydym/1"
                        class="shopping-bag__see"
                      >
                        <img src={eye} alt="eye" />
                      </Link>
                    </div>
                    <div class="shopping-bag__table">
                      <table>
                        <tbody>
                          <tr>
                          <td>{dil==="TM"?"Sargyt":(dil==="RU"?"Заказ":"Order")} №</td>
                            <td>#{props.order.id}</td>
                          </tr>
                          <tr>
                          <td>{dil==="TM"?"Satyn alyjy":(dil==="RU"?"Покупатель":"Buyer")}</td>
                            <td>{props.order.User.fname} {props.order.User.lastname}</td>
                          </tr>
                          <tr>
                            <td>{dil==="TM"?"Harytlaryň sany":(dil==="RU"?"Количество товаров":"Number of products")}</td>
                            <td>{props.order.sany}</td>
                          </tr>
                          { props?.order?.cashBack && <tr>
                            <td>CashBack</td>
                            <td>{props?.order?.cashBackMoney} TMT</td>
                          </tr> }
                          <tr>
                          <td>{dil==="TM"?"Jemi":(dil==="RU"?"Сумма":"Sum")}</td>
                            <td>{props.order.sum} TMT</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </article>
                  
               
    )
}

export default Sargytlarym;