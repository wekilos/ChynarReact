import React,{ useState, useContext } from "react";

import "../../css/style.css";

import home from "../../img/icons/home.svg";
import user from "../../img/icons/user.svg";
import phoneNumber from "../../img/icons/phone-number.svg";
import trash from "../../img/icons/trash.svg";
import edit from "../../img/icons/edit.svg";
import { message } from "antd";
import { axiosInstance } from "../../utils/axiosIntance";
import { SebedimContext } from "../../context/Sebedim";

const Salgylarym = (props)=>{

  const { dil } =useContext(SebedimContext)
  const [editBool,setEditBool] = useState(false);
  const [mainAddress,setMainAddress] = useState(false);
  const [adres,setAdres] = useState();

  const ShowInner = (add)=>{
    console.log(add);
    setAdres(add);
    setEditBool(!editBool);
    // props.ShowAddressInner()
  }

  const SaveChange = (id)=>{
    setEditBool(false);
    axiosInstance.patch("/api/user/address/update/"+id,{
      rec_name:adres.rec_name,
      rec_number:adres.rec_number,
      rec_address:adres.rec_address,
    }).then((data)=>{
      console.log(data.data);
      message.success(dil==="TM"?"Üstünlikli üýgedildi!":(dil==="RU"?"Успешно модифицировано!":"Successfully modified!"));
      props.getAdress(props.profile.id)
    }).catch((err)=>{
      console.log(err);
    })
  }

  const DeleteAddress = (id)=>{
    axiosInstance.delete("/api/user/address/delete/"+id).then((data)=>{
      message.success(dil==="TM"?`Aýyryldy!`:(dil==="RU"?"Удалено!":"Deleted!"));
      props.getAdress(props.profile.id)
    }).catch((err)=>{
      console.log(err);
    })
  }
    return(
      <React.Fragment>
                  {!editBool && <article class="my-adress-slider__column">
                    <div class="my-adress-slider__adress">
                      <div class="my-adress-slider__adress-box">
                        <span><img src={home} alt="" /></span>
                        <p>{props.adress.rec_address}</p>
                      </div>
                      <div class="my-adress-slider__adress-box">
                        <span><img src={user} alt="" /></span>
                        <p>{props.adress.rec_name}</p>
                      </div>
                      <div class="my-adress-slider__adress-box">
                        <span
                          ><img src={phoneNumber} alt=""
                        /></span>
                        <p>+{props.adress.rec_number}</p>
                      </div>
                    </div>
                    <div class="my-adress-slider__bot">
                      <div class="my-adress-slider__checkbox">
                        <label onClick={()=>setMainAddress(!mainAddress)} for="1">
                          <input type="checkbox" id="1" checked={props.main} />
                          <span></span>
                          <p>{dil==="TM"?"Esasy salgy":(dil==="RU"?"Основной адрес":"Main address")}</p>
                        </label>
                      </div>
                      <div class="my-adress-slider__buttons">
                        <button onClick={()=>DeleteAddress(props.adress.id)} class="my-adress-slider__button">
                          <img src={trash} alt="" />
                        </button>
                        <a
                        onClick={()=>ShowInner(props.adress)}
                          // href="./profile-my-adresses-innner.html"
                          class="my-adress-slider__button"
                        >
                          <img src={edit} alt="" />
                        </a>
                      </div>
                    </div>
                  </article>}
                  {editBool && <form class="profile__input-box">
                    <div class="profile__input-box-inputs">
                      <div class="profile__title">{dil==="TM"?"Meniň salgym":(dil==="RU"?"Мой адрес":"My address")}</div>
                      <div class="profile__input">
                        <input value={adres.rec_name} onChange={(e)=>setAdres({...adres,rec_name:e.target.value})} type="text" placeholder="Adyňyz" />
                      </div>
                      <div class="profile__input">
                        <input value={adres.rec_address} onChange={(e)=>setAdres({...adres,rec_address:e.target.value})} type="text" placeholder="Salgyňyz" />
                      </div>
                      <div class="profile__input">
                        <input value={adres.rec_number} onChange={(e)=>setAdres({...adres,rec_number:e.target.value})} type="phone" placeholder="Telefon belgiňiz" />
                      </div>
                    </div>
                    <div class="profile__submit-btn">
                      <input  onClick={()=>SaveChange(adres.id)} type="button" value={dil==="TM"?"Ýatda sakla":(dil==="RU"?"Запомнить":"Remember")} />
                    </div>
                  </form>}
                  </React.Fragment>
                
    )
}

export default Salgylarym;