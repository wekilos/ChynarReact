import { message } from "antd";
import React, { useState,useEffect,useContext } from "react";
import { Link } from "react-router-dom";

import "../../css/style.css";
import Salgylarym from "./salgylarym";
import Sargydym from "./Sargydym";
import Sargytlarym from "./sargytlarym";

import shappinBag2 from "../../img/icons/profile/shopping-bag2.svg";
import shappinBag1 from "../../img/icons/profile/shopping-bag1.svg";
import loc1 from "../../img/icons/profile/loc1.svg";
import loc2 from "../../img/icons/profile/loc2.svg";
import padLoc1 from "../../img/icons/profile/padlock1.svg";
import padLoc2 from "../../img/icons/profile/padlock2.svg";
import logout1 from "../../img/icons/profile/logout1.svg";
import logout2 from "../../img/icons/profile/logout2.svg";
import ProfilePassword from "./profilePassword";
import { logout } from "../../utils";
import { axiosInstance } from "../../utils/axiosIntance";
import { SebedimContext } from "../../context/Sebedim";

const Profile = (props)=>{

  const {dil} = useContext(SebedimContext);

    const [sargytlar,setSargytlar] = useState("active");
    const [sargydym,setSargydym] = useState(false);
    const [order,setOrder] = useState();
    const [salgylar,setSalgylar] = useState("");
    const [acharsozi,setAcharsozi] = useState("");
    const [cashBack,setCashBack] = useState("");
    const [editBoolPrrof,setEditBoolPrrof] = useState(false);
    const [ profile, setProfile ] = useState();
    const [ orders,setOrders] = useState([]);
    const [adresses,setAddresses] = useState([])
    const [newAddress,setNewAddress] = useState(false);
    const [rec_name,setrec_name] = useState();
    const [rec_address,setrec_address] = useState();
    const [rec_number,setrec_number] = useState();

    
  useEffect(()=>{
        let UserProfile = JSON.parse(localStorage.getItem("ChynarProfile"));
        // console.log(UserProfile)
        setProfile(UserProfile);
    getOrders(UserProfile.id);
    getAdress(UserProfile.id);
  },[]);

  const getOrders = (id)=>{
    axiosInstance.get("/api/orders/"+id).then((data)=>{
      // console.log(data.data);
      setOrders(data.data);
    }).catch((err)=>{
      console.log(err);
    });
  }

  const getAdress = (id)=>{
    axiosInstance.get("/api/user/address/"+id).then((data)=>{
      // console.log(data.data);
      setAddresses(data.data);
    }).catch((err)=>{
      console.log(err);
    })
  }
    const ShowSargydym = (order)=>{
      setSargydym(!sargydym);
      setOrder(order)
    }

    const ShowAddressInner = ()=>{
      setEditBoolPrrof(!editBoolPrrof)
    }

    const SideBar = (id)=>{
        if(id===1){
            setSargytlar("active");
            setSargydym(false);
            setEditBoolPrrof(false)
        }else{
            setSargytlar("")
        }
        if(id===2){
            setSalgylar("active");
            setSargydym(false)
            setEditBoolPrrof(false);
            setNewAddress(false)
        }else{
            setSalgylar("")
        }
        if(id===3){
            setAcharsozi("active");
            setSargydym(false)
            setEditBoolPrrof(false)
        }else{
            setAcharsozi("")
        }
        if(id===4){
          setCashBack("active");
          setSargydym(false);
          setEditBoolPrrof(false)
        }else{
          setCashBack("")
        }
    }

    const NewAddress = ()=>{
      axiosInstance.post("/api/user/address/create/"+profile.id,{
        rec_name:rec_name,
        rec_number:rec_number,
        rec_address:rec_address,
      }).then((data)=>{
        // console.log(data.data);
        message.success("Successfully!")
        setrec_number("");
        setrec_name("");
        setrec_address("")
        setNewAddress(false);
        getAdress(profile.id)
      }).catch((err)=>{
        console.log(err);
      })
    }
    return (
        <main class="main">
        <div class="profile">
          <div class="profile__container __container">
            <div class="profile__row">
              <div class="profile__controllers">
                <div class="profile__controllers-menu">{dil==="TM"?"Menu":(dil==="RU"?"Меню":"Menu")}</div>
                <div class="profile__controllers-box">
                  
                  <Link onClick={()=>SideBar(1)} to="/profile" className={sargytlar}>
                    <div class="profile__controllers-img">
                      <img src={shappinBag1} alt="" />
                      <img
                        src={shappinBag2}
                        alt=""
                        class="active"
                      />
                    </div>
                    <p>{dil==="TM"?"Meniň sargytlarym":(dil==="RU"?"Мои заказы":"My orders")}</p>
                  </Link>

                  <Link onClick={()=>SideBar(2)} to="/profile" className={salgylar}>
                    <div class="profile__controllers-img">
                      <img src={loc1} alt="" />
                      <img
                        src={loc2}
                        alt=""
                        class="active"
                      />
                    </div>
                    <p>{dil==="TM"?"Meniň salgylarym":(dil==="RU"?"Мои адреса":"My addresses")}</p>
                  </Link>

                  <Link onClick={()=>SideBar(3)} to="/profile" className={acharsozi}>
                    <div class="profile__controllers-img">
                      <img src={padLoc1} alt="" />
                      <img
                        src={padLoc2}
                        alt=""
                        class="active"
                      />
                    </div>
                    <p>{dil==="TM"?"Açarsözi":(dil==="RU"?"Пароль":"Password")}</p>
                  </Link>

                  {false &&<Link onClick={()=>SideBar(4)} to="/profile" className={cashBack}>
                    <div class="profile__controllers-img">
                      <img src={shappinBag1} alt="" />
                      <img
                        src={shappinBag2}
                        alt=""
                        class="active"
                      />
                    </div>
                    <p>CashBack</p>
                  </Link>}

                  <Link onClick={()=>logout()} to="/profile">
                    <div class="profile__controllers-img">
                      <img src={logout1} alt="" />
                      <img
                        src={logout2}
                        alt=""
                        class="active"
                      />
                    </div>
                    <p>{dil==="TM"?"Çykmak":(dil==="RU"?"Выход":"Exit")}</p>
                  </Link>

                </div>
              </div>
                      {!sargydym && sargytlar==="active" && <div class="profile__shopping-bag shopping-bag">
                        <section class="shopping-bag__row">
                          {orders && orders.map((order)=>{
                            return <Sargytlarym ShowSargydym={ShowSargydym} order={order}/>
                          })}
                        </section>
                      </div>}
                      { sargydym && sargytlar==="active" && <Sargydym order={order}/>}




                      { salgylar==="active" && !newAddress && !editBoolPrrof &&  <div class="profile__my-adress-slider my-adress-slider">
                          <section class="my-adress-slider__row __js-slider-3">
                            {adresses && adresses.map((adress)=>{
                              return  <Salgylarym adress={adress} getAdress={getAdress} profile={profile} ShowAddressInner={ShowAddressInner} main={true}/>
                            })}
                            {/* <Salgylarym ShowAddressInner={ShowAddressInner} main={false}/> */}
                          </section>
                          <div class="my-adress-slider__add-adress">
                            <a onClick={()=>setNewAddress(!newAddress)}>{dil==="TM"?"Täze salgy goş":(dil==="RU"?"добавить новый адрес":"Add a new address")}</a>
                          </div>
                        </div>}
                        {salgylar==="active" && newAddress && <form class="profile__input-box">
                          <div class="profile__input-box-inputs">
                            <div class="profile__title">{dil==="TM"?"Täze salgym":(dil==="RU"?"мой новый адрес":"My new address")}</div>
                            <div class="profile__input">
                              <input value={rec_name} onChange={(e)=>setrec_name(e.target.value)} type="text" placeholder={dil==="TM"?"Adyňyz":(dil==="RU"?"Ваше имя":"Your name")} />
                            </div>
                            <div class="profile__input">
                              <input value={rec_address} onChange={(e)=>setrec_address(e.target.value)} type="text" placeholder={dil==="TM"?"Salgyňyz":(dil==="RU"?"Ваш адрес":"Your address")} />
                            </div>
                            <div class="profile__input">
                              <input value={rec_number} onChange={(e)=>setrec_number(e.target.value)} type="phone" placeholder={dil==="TM"?"Telefon belgiňiz":(dil==="RU"?"Ваш номер телефона":"Your phone number")}/>
                            </div>
                          </div>
                          <div class="profile__submit-btn">
                            <input onClick={()=>NewAddress()} type="button" value={dil==="TM"?"Ýatda sakla":(dil==="RU"?"Запомнить":"Remember")} />
                          </div>
                        </form>}


                        {
                         acharsozi==="active" && <ProfilePassword />
                        }

                        {
                          cashBack==="active"  && <h1>CashBacks</h1>
                        }

            </div>
          </div>
        </div>
      </main>
    )
}

export default Profile;