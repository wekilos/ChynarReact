import React, { useContext } from "react";

import "../../css/style.css";
import { SebedimContext } from "../../context/Sebedim";

const Form = ()=>{
  const { dil } = useContext(SebedimContext)
    return (
        <form class="profile__input-box">
                <div class="profile__input-box-inputs">
                  <div class="profile__title">{dil==="TM"?"Kontakt maglumatlary":(dil==="RU"?"Контактная информация":"Contact details")}</div>
                  <div class="profile__input">
                    <input type="text" placeholder={dil==="TM"?"Adyňyz":(dil==="RU"?"Ваше имя":"Your name")} />
                  </div>
                  <div class="profile__input">
                    <input type="text" placeholder={dil==="TM"?"Familýaňyz":(dil==="RU"?"Ваша фамилия":"Your surname")} />
                  </div>
                  <div class="profile__input">
                    <input type="phone" placeholder={dil==="TM"?"Telefon belgiňiz":(dil==="RU"?"Ваш номер телефона":"Your phone number")} />
                  </div>
                </div>
                <div class="profile__submit-btn">
                  <input type="submit" value={dil==="TM"?"Ýatda sakla":(dil==="RU"?"Запомнить":"Remember")} />
                </div>
              </form>
    )
}

export default Form;