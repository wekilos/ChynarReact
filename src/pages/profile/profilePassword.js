import { message } from "antd";
import React, { useState,useEffect,useContext } from "react";
import { SebedimContext } from "../../context/Sebedim";

import "../../css/style.css";
import { axiosInstance } from "../../utils/axiosIntance";
import Profile from "./profile";

const ProfilePassword = ()=>{

  const { dil } = useContext(SebedimContext)
  const [password,setPassword] = useState();
  const [newPassword,setNewPassword] = useState();
  const [profile,setProfile] = useState()

  useEffect(()=>{
    let UserProfile = JSON.parse(localStorage.getItem("ChynarProfile"));
    setProfile(UserProfile);
  },[])

    const ChangePassword = ()=>{
      if(password===newPassword){
          axiosInstance.post("/api/user/update/"+profile.id,{
            password:password,
          }).then((data)=>{
            console.log(data.data);
            message.success(dil==="TM"?"Üstünlikli üýgedildi!":(dil==="RU"?"Успешно модифицировано!":"Successfully modified!"));
            setNewPassword("")
            setPassword("");
          }).catch((err)=>{
            console.log(err);
          })
      }else{
        message.warn(dil==="TM"?"Tassyklaýyş kodyňyz nädogry!":(dil==="RU"?"Ваш проверочный код неверен!":"Your verification code is incorrect!"))
      }
    }
    return(
        <form class="profile__input-box">
                <div class="profile__input-box-inputs">
                  <div class="profile__input">
                    <input value={password} onChange={(e)=>setPassword(e.target.value)} type="text" placeholder={dil==="TM"?"Täze açarsözüni giriziň":(dil==="RU"?"Введите новый пароль":"Enter a new password")} />
                  </div>
                  <div class="profile__input">
                    <input value={newPassword} onChange={(e)=>setNewPassword(e.target.value)} type="text" placeholder={dil==="TM"?"Açarsözüňizi tassyklaň":(dil==="RU"?"Подтвердите свой пароль":"Confirm your password")} />
                  </div>
                </div>
                <div class="profile__submit-btn">
                  <input onClick={()=>ChangePassword()} type="button" value={dil==="TM"?"Ýatda sakla":(dil==="RU"?"Запомнить":"Remember")} />
                </div>
              </form>
    )
}

export default ProfilePassword;