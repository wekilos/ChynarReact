import { message } from "antd"
import React,  { useContext, useState }  from "react"
import "../../css/style.css"
import { axiosInstance } from "../../utils/axiosIntance"
import {SebedimContext} from '../../context/Sebedim'
const Contact = ()=>{

    const { dil } = useContext(SebedimContext)
    const [tema,setTema] = useState("");
    const [text,setText] = useState("");

let user = JSON.parse(localStorage.getItem("ChynarProfile"));
    const SendPost = ()=>{
        !user && message.warn("Ulgama girmeli!")
        user &&  axiosInstance.post("/api/post/create/"+user.id,{
            slug_tm:tema,
            description_tm:text,
        }).then((data)=>{
            message.success("Successfully");
            setTema("");
            setText("");
        }).catch((err)=>{
            console.log(err);
        })
    }


    return (
        <div>
            <main class="main">
                <section class="input-form">
                <div class="input-form__container __container">
                    <div class="input-form__form">
                    <div id="habarlar" class="input-form__title">{dil==="TM"?"Habar Ýaz":(dil==="RU"?"Напиши сообщение":"Write a message")}</div>
                    <div class="input-form__title" id="fail" style={{display:"none","color": "red",fontSize: "16px",fontWeight: "600",marginTop: "10px",marginBottom: "-30px"}}> Telefon belgiňiz ýa-da passwordyňyz nädogry! </div>
                    <div class="input-form__input--form">
                        <div class="input-form__input">
                        <input onChange={(e)=>setTema(e.target.value)} value={tema} style={{fontSize:"16px"}} id="tema" type="text" placeholder={dil==="TM"?"Habar Temasy":(dil==="RU"?"Тема сообщения":"Message Topic")} />
                        </div>
                        <div class="input-form__input input-form__input--password">
                        <textarea
                        onChange={(e)=>setText(e.target.value)} value={text}
                        id="text"
                        style={{border:"0.5px solid rgb(224, 215, 215)",borderRadius: "10px", 
                        "color": "rgb(109, 106, 106)",padding:"20px",fontSize:"16px"}}
                        class="input-form__input input-form__input--password"
                        cols="20" rows="10" maxLength={250}>

                        </textarea>
                        
                        </div>
                    </div>
                    

                    <div class="input-form__button">
                        <input onClick={()=>SendPost()} id="ugratButton"  type="button" value={dil==="TM"?"Habar Ugrat":(dil==="RU"?"Отправить сообщение":"Send Message")}  />
                    </div>
                    
                    </div>
                </div>
                </section>
            </main>
        </div>
    )
}

export default Contact;