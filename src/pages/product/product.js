import React, { useContext, useEffect, useRef, useState } from "react";
import { Link,useHistory,useParams } from "react-router-dom";
import Cart from "../../components/cart";

import "../../css/style.css";
import { Carousel, message } from "antd";
import "antd/dist/antd.css";

import surat from "../../img/slider/1.png";
import surat2 from "../../img/slider/2.png";
import serpay from "../../img/main/serpay.png";
import eye from "../../img/icons/eye.svg"
import heart from "../../img/icons/heart.svg";
import heartLike from "../../img/icons/heart-liked.svg";
import { axiosInstance, BASE_URL } from "../../utils/axiosIntance";
import { SebedimContext } from "../../context/Sebedim";

const Product = (props)=>{
  let { id } = useParams();
  const history = useHistory();
  const {dil,AddTo} = useContext(SebedimContext);
    const slider = useRef(null);
    const [like,setLike] = useState("added");
    const [product,setProduct] = useState({});
    const [proRenk,setProRenk] = useState(null);
    const [proRazmer,setProRazmer] = useState(null);
    const [day,setDay] = useState(0);
    const [hour,setHour] = useState(0);
    const [minut,setMinut] = useState(0);
    const [kategoryProducts,setKategoryProducts] = useState([])
    const marketId = localStorage.getItem("MarketId");
    
  const [pageNo,setPageNo] = useState(0);

    useEffect(()=>{
      getProduct(id);
      WatchedPro()
      console.log(dil)
      console.log(id);
    },[])

    useEffect(()=>{
      // product && getKategoryProducts(product?.MarketSubKategoriyaId);
    },[pageNo]);

    useEffect(()=>{
      const time = setTimeout(()=>{
        if(product.is_sale) {
          countdown()
        }
      },60000)
      return ()=>clearTimeout(time)
    },[ minut]);

    const WatchedPro = ()=>{
      axiosInstance.patch("/api/product/viev/"+id).then((data)=>{
        // message.success(data.data);
      }).catch((err)=>{
        console.log(err);
      })
    }
    const getProduct = (id)=>{
      axiosInstance.get("/api/product/"+id).then((data)=>{
        console.log("product",data.data)
        setProduct(data.data);
        if(data.data.is_sale){
          let month = parseInt(data.data.sale_until.slice(5,7));
          let day = parseInt(data.data.sale_until.slice(8,10));
          let date = new Date();
          if(month===1 && date.getMonth()+1===12){
            setDay(day+1+31-date.getDay());
          }else if(month>date.getMonth()+1){
            setDay(day+1+31-date.getDay());
          }else{
            setDay(day+2-date.getDay());
            console.log(day,date.getDate())
          }
          // let hour = parseInt(data.data.sale_until.slice(11,13));
          setHour(24-date.getHours())
          // let min = parseInt(data.data.sale_until.slice(14,16));
          setMinut(60-date.getMinutes())
          }

          
           getKategoryProducts(data.data.id) 
        
      }).catch((err)=>{
        console.log(err);
      })
    }


    const getKategoryProducts = (id)=>{
      axiosInstance.get(`/api/product/same/${id}`).then((data)=>{
        
        setKategoryProducts(data.data);
        console.log("kategory products",data.data)
      }).catch((err)=>{
        console.log(err);
      })
    }


    const iLike = ()=>{
        if(like==="added"){
            setLike("")
        }else{
            setLike("added")
        }
    }

    const countdown = (product)=> {
      if (day>0 || hour>0 || minut>0) {  
        if (day === 0) {
          setDay(0)
        }
        if (hour === 0 && day>0) {
          setHour(23)
          setDay(day-1)
        }
        if (minut === 0 && hour>0) {
          setMinut(59)
          setHour(hour-1)
        }
        if(minut>0){
          
          setMinut(minut-1)
        }
        
          
          
          
      }
    }
    
    const AddtoSebet = ()=>{
      if(product.Razmerlers.length>0 && product.Renklers.length>0){
          if(proRazmer){
            // proRenk && message.success(dil=="TM"?"Sebede gosyldy":dil=="RU"?"ДОБАВИТЬ В КОРЗИНУ":"Added to Basket")
          }else{
            message.warn(dil==="TM"?"Razmer saylan!":(dil==="RU"?"Выберите размер!":"Choose a size!"))
          }
          if(proRenk){
            // proRazmer && message.success(dil=="TM"?"Sebede gosyldy":dil=="RU"?"ДОБАВИТЬ В КОРЗИНУ":"Added to Basket")
          }else{
           message.warn(dil==="TM"?"Renk saylan!":(dil==="RU"?"Выберите цвет!":"Choose a color!"))
          }
          if(proRenk && proRazmer){
                message.success(dil=="TM"?"Sebede gosyldy":dil=="RU"?"ДОБАВИТЬ В КОРЗИНУ":"Added to Basket")
                AddTo(product,proRenk,proRazmer)
          }
      }else if(product.Razmerlers.length>0){
        if(!proRazmer){
          message.warn(dil==="TM"?"Razmer saylan!":(dil==="RU"?"Выберите размер!":"Choose a size!"))
        }
        if(proRazmer){
          message.success(dil=="TM"?"Sebede gosyldy":dil=="RU"?"ДОБАВИТЬ В КОРЗИНУ":"Added to Basket");
          AddTo(product,null,proRazmer)
           }
      }else if(product.Renklers.length>0){
        if(!proRenk){
          message.warn(dil==="TM"?"Renk saylan!":(dil==="RU"?"Выберите цвет!":"Choose a color!"))
        }
        if(proRenk){
          message.success(dil=="TM"?"Sebede gosyldy":dil=="RU"?"ДОБАВИТЬ В КОРЗИНУ":"Added to Basket");
          AddTo(product,proRenk,null)
           }
      }
    }

    return(
        <main class="main">
        <section class="main-second-box">
          <section class="bred-crumb">
            <div class="bred-crumb__container __container">
              <div class="bred-crumb__links">
                <Link to="/home" class="bred-crumb__link">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="15"
                    height="15"
                    viewBox="0 0 15 15"
                  >
                    <path
                      id="noun_Home_2102808"
                      d="M7.028,12.906l.327-.276v7.745A.625.625,0,0,0,7.98,21H19.02a.625.625,0,0,0,.625-.625V12.63l.327.276a.625.625,0,0,0,.806-.955l-3-2.533V7.039a.625.625,0,1,0-1.25,0V8.363L13.9,6.147a.625.625,0,0,0-.806,0l-6.875,5.8a.625.625,0,1,0,.806.955ZM12.1,19.75V15.238H14.9V19.75ZM13.5,7.443l4.895,4.132V19.75H16.147V14.613a.625.625,0,0,0-.625-.625H11.478a.625.625,0,0,0-.625.625V19.75H8.605V11.575Z"
                      transform="translate(-6 -6)"
                    />
                  </svg>
                </Link>
                <Link to={`/market/${marketId}`}  class="bred-crumb__link">
                  {dil && dil==="TM"?product?.Market?.name_tm:(dil==="RU"?product.Market&& product.Market.name_ru:product.Market&& product.Market.name_en)}
                </Link>
                <Link to={`/kategory/${product?.MarketKategoriya?.id}`} class="bred-crumb__link">
                  {dil && dil==="TM"?product.MarketKategoriya && product.MarketKategoriya.name_tm:(dil==="RU"?product.MarketKategoriya && product.MarketKategoriya.name_ru:product.MarketKategoriya && product.MarketKategoriya.name_en)}
                </Link>
                <Link to={`/subkategory/${product?.MarketSubKategoriya?.id}`} class="bred-crumb__link">
                  {dil && dil==="TM"?product.MarketSubKategoriya && product.MarketSubKategoriya.name_tm:(dil==="RU"?product.MarketSubKategoriya && product.MarketSubKategoriya.name_ru:product.MarketSubKategoriya && product.MarketSubKategoriya.name_en)}
                  </Link>
                <Link to={`/product/${product.id}`} class="bred-crumb__link">
                  {dil && dil==="TM"?product.name_tm:(dil==="RU"?product.name_ru:product.name_en)}
                </Link>
              </div>
            </div>
          </section>
          <div class="product-item">
            <div class="product-item__container __container">
              <div class="product-item__row">
                <section class="product-item__slider product-slider">
                  {product.is_sale &&<div class="product-slider__sale">{product.is_sale && (100-product.sale_price*100/product.price).toFixed(2)} %</div>}
                  <section class="product-slider__vertical">
                    <div
                      class="product-slider__vertical-wrapper swiper-wrapper"
                    >
                      {product.surat && <article
                        class="product-slider__vertical-slide swiper-slide"
                      >
                        <img src={product.surat && (BASE_URL+"/"+product.surat)} alt="" />
                      </article>}
                      {product.surat1 && <article
                        class="product-slider__vertical-slide swiper-slide"
                      >
                        <img src={product.surat1 && (BASE_URL+"/"+product.surat1)} alt="" />
                      </article>}
                      {product.surat2 && <article
                        class="product-slider__vertical-slide swiper-slide"
                      >
                        <img src={product.surat2 && (BASE_URL+"/"+product.surat2)} alt="" />
                      </article>}
                      
                     {product.surat3 && <article
                        class="product-slider__vertical-slide swiper-slide"
                      >
                        <img src={product.surat3 && (BASE_URL+"/"+product.surat3)} alt="" />
                      </article>}
                    </div>
                  </section>
                  <section class="product-slider__horizontal swiper-container ">
                    <div
                    //   class="product-slider__horizontal-wrapper swiper-wrapper"
                    // className=" main-slider__slides __js-slider"
                    >
                <Carousel autoplay  arrows ref={slider} dots>
                      {product.surat && <article
                        class="product-slider__horizontal-slide swiper-slide"
                      >
                        <img src={product.surat && (BASE_URL+"/"+product.surat)} alt="" />
                      </article>}
                      {product.surat1 && <article
                        class="product-slider__horizontal-slide swiper-slide"
                      >
                        <img src={product.surat1 && (BASE_URL+"/"+product.surat1)} alt="" />
                      </article>}
                      {product.surat2 && <article
                        class="product-slider__horizontal-slide swiper-slide"
                      >
                        <img src={product.surat2 && (BASE_URL+"/"+product.surat2)} alt="" />
                      </article>}
                      {product.surat3 && <article
                        class="product-slider__horizontal-slide swiper-slide"
                      >
                        <img src={product.surat3 && (BASE_URL+"/"+product.surat3)} alt="" />
                      </article>}
                      </Carousel>
                    </div>
                    <div onClick={() => slider.current.prev()} class="swiper-button-prev">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="18.828"
                        height="10.414"
                        viewBox="0 0 18.828 10.414"
                      >
                        <path
                          id="arrow"
                          d="M0,0,8,8l8-8"
                          transform="translate(1.414 1.414)"
                          fill="none"
                          stroke="#d1d1d6"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-miterlimit="10"
                          stroke-width="2"
                        />
                      </svg>
                    </div>
                    <div onClick={() => slider.current.next()} class="swiper-button-next">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="18.828"
                        height="10.414"
                        viewBox="0 0 18.828 10.414"
                      >
                        <path
                          id="arrow"
                          d="M0,0,8,8l8-8"
                          transform="translate(1.414 1.414)"
                          fill="none"
                          stroke="#d1d1d6"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-miterlimit="10"
                          stroke-width="2"
                        />
                      </svg>
                    </div>
                    
                  </section>
                </section>
                <div class="product-item__content">
                  <button onClick={iLike} class={`product-item__like like  ${like}`}>
                    <img
                      src={heart}
                      alt="heart"
                      class="no-liked"
                    />
                    <img
                      src={heartLike}
                      alt="heart"
                      class="liked"
                    />
                  </button>
                  <div class="product-item__name">{dil==="TM"?product.name_tm:(dil==="RU"?product.name_ru:product.name_en)}</div>
                  <article class="product-item__box">
                    <div class="product-item__model">Model № {product.id}</div>
                    <div class="product-item__views">
                      <img src={eye} alt="eye" />
                      <span>{product.view_count} reviews</span>
                    </div>
                  </article>
                  <article class="product-item__box">
                    <div class="product-item__price">
                      {product.is_sale && <div class="product-item__last-price">
                        <p>{product.is_valyuta_price?product.Config.currency_exchange*product.price:product.price}</p>
                        <span>&nbsp;TMT</span>
                      </div>}
                      <div
                        class="
                          product-item__current-price
                          product-item__current-price--sale
                        "
                      >
                        <p>{product.is_sale?(product.is_valyuta_price?product.Config.currency_exchange*product.sale_price:product.sale_price)
                        :(product.is_valyuta_price?product.Config.currency_exchange*product.price:product.price)}</p>
                        <span>&nbsp;TMT</span>
                      </div>
                    </div>
                    {product.is_sale && <div class="product-item__countdown countdown">
                      <div class="countdown__index"><span>{day}</span> {dil==="TM"?"gün":(dil==="RU"?"":"day")}</div>
                      <div class="countdown__index"><span>{hour}</span> {dil==="TM"?"sag":(dil==="RU"?"":"hour")}</div>
                      <div class="countdown__index"><span>{minut}</span> {dil==="TM"?"min":(dil==="RU"?"":"min")}</div>
                    </div>}
                  </article>
                  {product?.Razmerlers && <div className="razmerler">
                        {product?.Razmerlers?.map((razmer)=>{
                          return razmer.active&&<span onClick={()=>setProRazmer(razmer.name_tm)} className={`${razmer.name_tm==proRazmer && "selectedRazmer"} `}>
                            {dil==="TM"?razmer.name_tm:(dil==="RU"?razmer.name_ru:razmer.name_en)}</span>
                        })}
                    </div>}
                  {product?.Renklers && <div className="razmerler">
                      {product?.Renklers?.map((renk)=>{
                        return renk.active&&<span onClick={()=>setProRenk(renk.name_tm)} className={`${renk.name_tm==proRenk && "selectedRazmer"} `}> 
                          {dil==="TM"?renk.name_tm:(dil==="RU"?renk.name_ru:renk.name_en)}</span>
                      })}
                  </div>}
                  <div class="product-item__add-product">
                  { product?.Brand && <img onClick={()=>history.push({pathname:`/brand/${product.BrandId}`})} width={100} height="60" style={{borderRadius:"16px",boxShadow:"0 1px 3px 1px grey",cursor:"pointer"}} src={BASE_URL+"/"+product?.Brand?.surat} />}                    <button onClick={()=>AddtoSebet()}>{dil==="TM"?"sebede goş":(dil==="RU"?"добавить в корзину":"add to cart")}</button>
                  </div>
                  <div class="product-item__info">
                    <div class="product-item__info-title">Info</div>
                    <div class="product-item__info-text">
                      {dil==="TM"?product.description_tm:(dil==="RU"?product.description_ru:product.description_en)}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="products">
          <section class="products__top __container">
            <div class="products__top-title">{dil==="TM"?"Bu haryt bilen alynýanlar":(dil==="RU"?"Те, кто покупается с этим товаром":"Those who are bought with this commodity")}</div>
            <div class="products__top-see-all">
              <Link to={"/market/"+product?.Market?.id}>{dil==="TM"?"Hemmesini görkez":(dil==="RU"?"Показать все":"Show it all")}</Link>
            </div>
          </section>
          <div class="products__container __container">
            <section class="products__list">
              <section class="product">
              { kategoryProducts.map((product)=>{
                return  product.is_active && <div onClick={()=>getProduct(product.id)}><Cart  product={product}/></div>
              })}
             
               {/* <div style={{width:"100%",margin:"0 auto"}} class="product-item__add-product">
                    <button 
                    onClick={()=>setPageNo(pageNo+1)}
                    >
                      Dowamy
                      </button>
                  </div> */}
                  
              </section>
            </section>
          </div>
        </section>
      </main>
    )
}

export default Product;