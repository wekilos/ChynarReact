import React, { useContext, useEffect, useState } from "react";
import { Link, useHistory, useParams } from "react-router-dom";

import "../../css/style.css";

import Cart from "../../components/cart";
import { SebedimContext } from "../../context/Sebedim";
import { axiosInstance } from "../../utils/axiosIntance";

const Favourites = ()=>{
 
  
  let { id } = useParams();
  const history = useHistory()
  const {dil,favorites} = useContext(SebedimContext);
  const MarketId = localStorage.getItem("MarketId");
  const [products,setProducts] = useState([]);
  const [pageNo,setPageNo] = useState(0);
  const [is_have,setIs_have] = useState(false);

  useEffect(()=>{
    getKategoryProduct()
  },[pageNo]);

  const getKategoryProduct = ()=>{
    axiosInstance.get("/api/product/market/subKategory/"+id,{
      params:{
        page:pageNo
      }
    }).then((data)=>{
      console.log(data.data)
      if(data.data[0]){
        let array = products;
        for (let i=0; i < data.data.length; i++){
          array.push(data.data[i]);
        }
        setProducts([...array]);
      }else{
        setIs_have(true);
      }
      
    }).catch((err)=>{
      console.log(err);
    });
  }
    return (
        <main class="main">
        <section class="favorite">
          <section class="favorite__bred-crumb bred-crumb">
            <div class="bred-crumb__container __container">
              <div class="bred-crumb__links">
                <Link to="/home" class="bred-crumb__link">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="15"
                    height="15"
                    viewBox="0 0 15 15"
                  >
                    <path
                      id="noun_Home_2102808"
                      d="M7.028,12.906l.327-.276v7.745A.625.625,0,0,0,7.98,21H19.02a.625.625,0,0,0,.625-.625V12.63l.327.276a.625.625,0,0,0,.806-.955l-3-2.533V7.039a.625.625,0,1,0-1.25,0V8.363L13.9,6.147a.625.625,0,0,0-.806,0l-6.875,5.8a.625.625,0,1,0,.806.955ZM12.1,19.75V15.238H14.9V19.75ZM13.5,7.443l4.895,4.132V19.75H16.147V14.613a.625.625,0,0,0-.625-.625H11.478a.625.625,0,0,0-.625.625V19.75H8.605V11.575Z"
                      transform="translate(-6 -6)"
                    />
                  </svg>
                </Link>
                <Link to={`/market/${MarketId}`}  class="bred-crumb__link"
                  >{dil==="TM"?"Çynar market":(dil==="RU"?"Чынар маркет":"Cynar market")}
                </Link>
                <Link to={`/kategory/${products[0]?.MarketKategoriya?.id}`} class="bred-crumb__link"> 
                  {dil==="TM"?products[0]?.MarketKategoriya?.name_tm:(dil==="RU"?products[0]?.MarketKategoriya?.name_ru:products[0]?.MarketKategoriya?.name_en)}
                  </Link>
                <Link to={`/subkategory/${products[0]?.MarketSubKategoriya?.id}`}class="bred-crumb__link">
                  {dil==="TM"?products[0]?.MarketSubKategoriya?.name_tm:(dil==="RU"?products[0]?.MarketSubKategoriya?.name_ru:products[0]?.MarketSubKategoriya?.name_en)}
                </Link>
              </div>
            </div>
          </section>
          <section class="favorite__products products">
            <div class="products__container __container">
              <section class="products__list">
                <section class="product">
                  {
                   !products && is_have && <div style={{width:"100%", marginTop:"50px"}}>
                    <h1 style={{width:"100%",fontSize:"36px",color:"#1B2437",textAlign:"center"}}>
                      {dil=="EN"?"Sorry item not found!":(dil=="RU"?"Извините товар не найден!":"Bagyşlaň siziň harydyňyz tapylmady! ")}
                       </h1>
                    <div style={{width:"100%",display:"flex",justifyContent:"center"}}>
                    <button onClick={()=>{history.goBack()}} style={{width:"177px",height:"52px",borderRadius:"16px",backgroundColor:"#69A03A",color:"#fff",margin:"20px auto"}}> {dil=="EN"?"Go back":(dil=="RU"?"Вернитесь назад ":"Yza dolan")} </button>
                    </div>
                  </div>
                  }
                  {
                    products.map((product,i)=>{
                      return  <Cart key={i} product={product} />
                    })
                  }
                  {products[0] && !is_have &&  <div style={{width:"100%",margin:"0 auto"}} class="product-item__add-product">
                    <button 
                    onClick={()=>setPageNo(pageNo+1)}
                    >
                      {/* {dil==="TM"?"sebede goş":(dil==="RU"?"добавить в корзину":"add to cart")} */}
                      {dil==="TM"?"Dowamy":(dil==="RU"?"Продолжение":"Continued")}
                      </button>
                  </div>}
                </section>
              </section>
            </div>
          </section>
        </section>
      </main>
    )
}

export default Favourites;