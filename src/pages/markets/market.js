import React, { useContext, useEffect, useState } from "react";

import "../../css/style.css";
import { Avatar, Carousel, Drawer, List, Menu, message, Select, Skeleton, Slider } from "antd";
import "antd/dist/antd.css";
import {LoadingOutlined} from '@ant-design/icons'

import serpay from "../../img/main/serpay.png"
import kategory from "../../img/icons/category.svg";
import heart from "../../img/icons/heart.svg";
import heartLiked from "../../img/icons/heart-liked.svg";
import shoppingCart from "../../img/icons/shopping-cart.svg"
import filter from "../../img/download.png"
import { Link,useHistory,useParams } from "react-router-dom";
import Cart from "../../components/cart";
import { axiosInstance, BASE_URL } from "../../utils/axiosIntance";
import { SebedimContext } from "../../context/Sebedim";
import Sider from "antd/lib/layout/Sider";
import SubMenu from "antd/lib/menu/SubMenu";

const {Option} = Select;

const Market = ()=>{

  const history = useHistory()
  const {dil} = useContext(SebedimContext);
  const [sliders,setSliders] = useState([])
  const {idM} = useParams()
  const [marketKategory,setMarketKategory] = useState([]);
  const [products,setProducts] = useState([]);
  const [activeKategory,setActiveKategory] = useState(0);
  const [showFilter,setShowFilter] = useState(false);
  const [taze,setTaze] = useState(null);
  const [skitga,setSkitga] = useState(null);
  const [baha,setBaha] = useState(null);
  const [startBaha,setStartBaha] = useState(0);
  const [endBaha,setEndBaha] = useState(1000);
  const [ID,setID] = useState();
  const [which,setWich] = useState();
  const [loading,setLoading] = useState(false);
  const [ is_have, setIs_have] = useState(false);
  const [ dowamyLoading, setDowamyLoading ] = useState(false)

  const [pageNo,setPageNo] = useState(0);
  useEffect(()=>{
    let MarketId = localStorage.getItem("MarketId");
    // message.success(MarketId)
    getMarkets (idM);
    getMarketProducts(idM);
  },[idM]);

  useEffect(()=>{
    GetSliders();
  },[]);

  useEffect(()=>{
    if(which=="market"){
      getMarketProducts(ID,"dowam");
      
   }else if(which=="kategory"){
     getKategoryProduct(ID,"dowam");
     
   }else if(which=="subKategory"){
     getSubKategoryProduct(ID,0,"dowam")
   }
  },[pageNo])
  const getMarkets = (id)=>{
    axiosInstance.get("/api/market/"+id).then((data)=>{
      setMarketKategory(data.data.MarketKategoriyas);
      console.log("kategories",data.data)
    }).catch((err)=>{
      console.log(err);
    })
  }

  const getMarketProducts = (id,dowam)=>{
    dowam==="dowam" ? setDowamyLoading(true) : setLoading(true);
    setID(id);
    setWich("market");
    console.log("baha",baha,
      "startBaha",startBaha,
      "endBaha",endBaha,
      "is_new",taze,
      "is_sale",skitga)
    axiosInstance.get("/api/product/market/"+id,{
      params:{
        page:pageNo,
        baha:baha,
        startBaha:startBaha,
        endBaha:endBaha,
        is_new:taze,
        is_sale:skitga}
    }).then((data)=>{
      if(data.data[0]){
          setIs_have(true);
      }else{
        setIs_have(false);
      }
      setDowamyLoading(false)
      setLoading(false);
      console.log("products",data.data,"market");
      if(dowam=="dowam"){
        let array = products;
        for (let i=0; i < data.data.length; i++){
          array.push(data.data[i]);
        }
        setProducts([...array]);
      }else{
        setProducts(data.data)
      }
      
    }).catch((err)=>{
      console.log(err);
      setLoading(false);
    })
  }

  const getKategoryProduct = (id,dowam)=>{
    dowam==="dowam" ? setDowamyLoading(true) : setLoading(true);
    setActiveKategory(id);
    setID(id);
    setWich("kategory");
    axiosInstance.get("/api/product/market/product/"+id,{
      params:{
        page:pageNo,
        baha:baha,
        startBaha:startBaha,
        endBaha:endBaha,
        is_new:taze,
        is_sale:skitga
      }
    }).then((data)=>{
      setLoading(false);
      setDowamyLoading(false);
      if(data.data[0]){
        setIs_have(true);
      }else{
        setIs_have(false);
      }
      console.log("products",data.data)
      if(dowam=="dowam"){ 
        let array = products;
        for (let i=0; i < data.data.length; i++){
          array.push(data.data[i]);
        }
        setProducts([...array]);
      }else{
        setProducts(data.data);
      }
      
    }).catch((err)=>{
      console.log(err);
      setLoading(false)
    });
  }

  const FilterHandler = ()=>{
    setShowFilter(!showFilter);
  }

  function onChange(value) {
    console.log('onChange: ', value);
  }
  
  function onAfterChange(value) {
    console.log('onAfterChange: ', value);
    setStartBaha(value[0]);
    setEndBaha(value[1]);
  }

  const SaveFilter = ()=>{
    FilterHandler()
    if(which=="market"){
       getMarketProducts(ID);
      console.log("issale",startBaha,endBaha)
       
    }else if(which=="kategory"){
      console.log("issale",startBaha,endBaha)
      getKategoryProduct(ID);
      
    }else if(which=="subKategory"){
      getSubKategoryProduct(ID)
    }else{
      message.worn("Name eddyan?")
    }
  }

  const ChangeNew = (value)=>{
    setTaze(value)
  }

  const ChangeSale = (value)=>{
    setSkitga(value)
  }

  const ChangeBaha = (value)=>{
    setBaha(value)
  }

  const GetSliders = ()=>{
    axiosInstance.get("/api/sliders").then((data)=>{
      setSliders(data.data);
      console.log("sliders",data.data);
    }).catch((err)=>{
      console.log(err);
    })
  }

  const getSubKategoryProduct = (id,activeId,dowam)=>{
    dowam==="dowam" ? setDowamyLoading(true) : setLoading(true);
    activeId && setActiveKategory(activeId);
    setID(id);
    setWich("subKategory");
    axiosInstance.get("/api/product/market/subKategory/"+id,{
      params:{
        page:pageNo,
        baha:baha,
        startBaha:startBaha,
        endBaha:endBaha,
        is_new:taze,
        is_sale:skitga
      }
    }).then((data)=>{
      setLoading(false);
      setDowamyLoading(false);
      
      if(data.data[0]){
        setIs_have(true);
      }else{
        setIs_have(false);
      }
      console.log("subKategory products",data.data)
      if(dowam=="dowam"){
        let array = products;
        for (let i=0; i < data.data.length; i++){
          array.push(data.data[i]);
        }
        setProducts([...array]);
      }else{
        setProducts(data.data);
      }
      
    }).catch((err)=>{
      console.log(err);
      setLoading(false)
    });
  }
    return(
        <main class="main">
        <section class="main-slider main-slider--mini">
          <div class="main-slider__container __container">
            <div class="main-slider__slides __js-slider">
            <Carousel autoplay  arrows >
                {/* {banners.map((banner)=>{
                  return ( */}

                    {sliders.map((slider)=>{
                      return <article className="main-slider__slide">
                                  <div  onClick={()=>{history.push({pathname:"/kategory/"+slider.link})}}  className="main-slider__img main-slider--mini ">
                                        {dil=="TM"?<img src={BASE_URL+"/"+slider.title_tm} alt="surat"/>:
                                        (dil=="RU"?<img src={BASE_URL+"/"+slider.title_ru} alt="surat"/>
                                        :<img src={BASE_URL+"/"+slider.title_en} alt="surat"/>)}
                                  </div>
                              </article>
                    })}
                        {/* <article  className="main-slider__slide ">
                            <div className="main-slider__img">
                                <img className="BannerImg"  src={serpay} alt=""/>
                            </div>
                        </article> */}
                        

                    {/* )
                })} */}

              </Carousel>
            </div>
          </div>
        </section>
        <section class="products">
          <div style={{position:"static"}}  class="products__container __container">
             <div style={{position:"sticky",top:"10px",zIndex:"100"}} class="products__category p-category">
                <Drawer
                height={500}
                title="Filter"
                visible={showFilter}
                width={320}
                onClose={FilterHandler}>

                    <Select
                    onChange={ChangeNew}
                    value={taze}
                    style={{width:"100%",marginBottom:"10px"}}>
                        <Option value={null}>Ahli Harytlar</Option>
                        <Option value={true}>Taze Harytlar</Option>
                        <Option value={false}>Taze dal harytlar</Option>
                    </Select>

                    <Select
                    onChange={ChangeSale}
                    value={skitga}
                    style={{width:"100%",marginBottom:"10px"}}>
                        <Option value={null}>Ahli Harytlar</Option>
                        <Option value={true}>Arzanladysdaky Harytlar</Option>
                        <Option value={false}>Arzanladysdaky dal harytlar</Option>
                    </Select>

                    <Select
                    onChange={ChangeBaha}
                    value={baha}
                    style={{width:"100%",marginBottom:"10px"}}>
                        <Option value={null}>Berilen Tertipde</Option>
                        <Option value={1}>Arzan Gymmada</Option>
                        <Option value={2}>Gymmatdan Arzana</Option>
                        <Option value={3}>Harydyn Ady A-Z</Option>
                        <Option value={4}>Harydyn Ady Z-A</Option>
                    </Select>
                    
                    <Slider
                        range
                        step={5}
                        max={1000}
                        defaultValue={[10, 1000]}
                        onChange={onChange}
                        onAfterChange={onAfterChange}
                      />
                      <div style={{width:"100%",display:"inline-flex",justifyContent:"space-between"}}>
                    <h1>0</h1>
                    <h1 style={{fontWeight:"bold"}}>{startBaha}-{endBaha}</h1>
                    <h1>1000</h1>
                    </div>


                    <div className="filterButtons">
                      <button onClick={()=>{SaveFilter()}} className="filterButton" >Tertiple</button>
                      <button onClick={()=>FilterHandler()} className="filterButton" >Goy bolsun</button>
                    </div>
                </Drawer>
              <div onClick={()=>FilterHandler()} class="p-category__title" style={{marginBottom:"5px",cursor:"pointer"}}>
                <span>
                  <img src={filter} alt="" style={{width:"20px",objectFit:"cover"}}/>
                </span>
                <p>{dil==="TM"?"Filter":(dil==="RU"?"Категория":"Category")}</p>
              </div>

              <div class="p-category__title">
                <span>
                  <img src={kategory} alt="" />
                </span>
                <p>{dil==="TM"?"Kategoriýalar":(dil==="RU"?"Категория":"Category")}</p>
              </div>
              <ul class="p-category__row">
                <Menu mode="inline" className="p-category__row">
                {marketKategory.map((kategory,i)=>{
                //  return console.log("kategory",kategory)
                  return kategory.active && !kategory.deleted && <SubMenu key={i}  title={ 
                          <li 
                          // onClick={()=>getKategoryProduct(kategory.id)}
                           class={`p-category__column ${activeKategory === kategory.id && "active"}  `}>  {/* active */}
                             <Link >{dil==="TM"?kategory.name_tm:(dil==="RU"?kategory.name_ru:kategory.name_en)}</Link>
                          </li>}
                          >
                            <Menu.Item onClick={()=>getKategoryProduct(kategory.id)}  className={`menuitem2 `} key={kategory.id}>
                                <Link class={` menuitem2 `} >{dil==="TM"?kategory.name_tm:(dil==="RU"?kategory.name_ru:kategory.name_en)}</Link>
                                </Menu.Item>
                            {kategory.MarketSubKategoriyas.map((subKategory)=>{
                                return subKategory.active && !subKategory.deleted && <Menu.Item onClick={()=>getSubKategoryProduct(subKategory.id,kategory.id)}  className={`menuitem2 `} key={subKategory.id}>
                                <Link class={` menuitem2 `} >{dil==="TM"?subKategory.name_tm:(dil==="RU"?subKategory.name_ru:subKategory.name_en)}</Link>
                                </Menu.Item>
                            })}
                           
                            {/* <Menu.Item className={`menuitem2 `} key={i+1}>
                            <Link class={`menuitem2 `}>{dil==="TM"?kategory.name_tm:(dil==="RU"?kategory.name_ru:kategory.name_en)}</Link>
                            </Menu.Item> */}
                        </SubMenu>
                })}
                
                </Menu>
              </ul>
            </div>
            <section style={{position:"static"}} class="products__list">
              <section class="product product--mini">
                {
                 !loading && products.map((product)=>{
                   return product.is_active && <Cart product={product}/>
                  })
                } 
                {
                  loading && <div style={{fontSize:"46px",marginTop:"30px",color:"green"}}>
                    <LoadingOutlined />
                  </div>
                }
                
              {products[0] && is_have && !dowamyLoading && <div style={{width:"100%",margin:"0 auto"}} class="product-item__add-product">
                    <button 
                    onClick={()=>setPageNo(pageNo+1)}
                    >
                      {/* {dil==="TM"?"sebede goş":(dil==="RU"?"добавить в корзину":"add to cart")} */}
                      Dowamy
                      </button>
                  </div>}

                  {
                  dowamyLoading && <div style={{fontSize:"46px",margin:"30px auto",color:"green"}}>
                    <LoadingOutlined />
                  </div>
                }
              </section>
            </section>
          </div>
        </section>
      </main>
    )
}

export default Market;