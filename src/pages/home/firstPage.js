import React, { useContext, useEffect, useState } from "react"

import "../../css/style.css";
import { axiosInstance } from "../../utils/axiosIntance";
import map from "../../img/c.jpg"
import { useHistory } from "react-router-dom";
import { SebedimContext } from "../../context/Sebedim";

const FirstPage = (props)=>{
    const history = useHistory();
    const {dil} = useContext(SebedimContext)
    const [data,setData] = useState([]);
    useEffect(()=>{
        GetWelayat();
    },[])
   const GetWelayat = ()=>{ 
       axiosInstance.get("/api/welayatlar").then((data)=>{
        console.log(data.data);
        localStorage.setItem("welayats",JSON.stringify(data.data))
        setData(data.data);
    }).catch((err)=>{
        console.log(err);
    })
}

const GoHome = (id)=>{
    history.push({
        pathname:"/home"
    })
    localStorage.setItem("welayatId",id)
}
const array = [
    {
        active: true,
        createdAt: "2022-01-15T04:09:36.371Z",
        deleted: false,
        id: 1,
        name_en: "Dashoguz",
        name_ru: "Дашогуз",
        name_tm: "Daşoguz",
        short_name_en: null,
        short_name_ru: null,
        short_name_tm: null,
        updatedAt: "2022-01-15T09:52:48.134Z"
    },
    {
        active: true,
        createdAt: "2022-01-19T05:26:48.176Z",
        deleted: false,
        id: 3,
        name_en: "Ashgabat",
        name_ru: "Ашгабат",
        name_tm: "Aşgabat",
        short_name_en: null,
        short_name_ru: null,
        short_name_tm: null,
        updatedAt: "2022-01-19T05:26:48.176Z",
    },{
        active: true,
        createdAt: "2022-01-30T12:49:43.268Z",
        deleted: false,
        id: 4,
        name_en: "Ahal",
        name_ru: "Ахал",
        name_tm: "Ahal",
        short_name_en: null,
        short_name_ru: null,
        short_name_tm: null,
        updatedAt: "2022-01-30T12:49:43.268Z",
    }
]

    return  (

        <div className="Welayatlar">
            <img src={map} alt=""/>
                {array && array.map((welayat)=>{
                     return <h1 onClick={()=>GoHome(welayat.id)} className="Welayat">{ dil==="TM"? welayat.name_tm:(dil==="RU"?welayat.name_ru : welayat.name_en)  } </h1>
                })}
        </div>
    )
}

export default FirstPage;