import React, { useContext, useEffect, useState } from "react";

import "../../css/style.css";
import { Carousel, Drawer, Menu, message, Select, Slider } from "antd";
import "antd/dist/antd.css";


import { Link,useHistory,useParams } from "react-router-dom";
import Cart from "../../components/cart";
import { axiosInstance, BASE_URL } from "../../utils/axiosIntance";
import { SebedimContext } from "../../context/Sebedim";

const {Option} = Select;

const SearchData = ()=>{

  const history = useHistory()
  const {dil} = useContext(SebedimContext);
  const {product_name} = useParams()
  const [products,setProducts] = useState([]);
  
  useEffect(()=>{
      getSearchProducts();
  },[product_name]);

 
  const getSearchProducts = ()=>{
      let welayatId = localStorage.getItem("welayatId")
      axiosInstance.get("/api/products/search",{
          params:{
            welayatId:welayatId,
            all:product_name
          }
      }).then((data)=>{
          console.log("searchdata",data.data)
        setProducts(data.data);
      }).catch((err)=>{
          console.log(err);
      })
  }



    return(
        <main class="main">
        <section class="products">
          <div style={{position:"static"}}  class="products__container __container">
             
            <section style={{position:"static"}} class="products__list">
              <section class="product product--mini">
                {
                  products.map((product)=>{
                   return product.is_active && <Cart key={"pro"+product.id} product={product}/>
                  })
                } 
             
              </section>
            </section>
          </div>
        </section>
      </main>
    )
}

export default SearchData;