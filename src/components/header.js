import React, { useContext, useEffect, useState }  from "react";

import {Link,useHistory} from "react-router-dom";

import logo from '../img/icons/logo.svg';
import search from "../img/icons/header/search.svg"
import user from "../img/icons/header/user.svg";
import shoppingCart from "../img/icons/header/shopping-cart.svg";
import heart from "../img/icons/header/heart1.svg"
import "../css/style.css";
import { SebedimContext } from "../context/Sebedim";
import { axiosInstance } from "../utils/axiosIntance";
import { message } from "antd";
const Header = ()=>{

  const history = useHistory();
  const {dil,ChangeDil,sebedim} = useContext(SebedimContext)

  let sany=0;
    sebedim.map((haryt)=>{
            sany=sany+haryt.sany;
            return null;
    });

    const [SearchInput,setSearchInput] = useState("");
    const [searchData,setSearchData]=useState("");
    const [menu,setMenu] = useState("")
    const [menuBool,setMenuBool] = useState(false);
    const [kategory,setKategory] = useState("")
    const [kategoryBool,setKategoryBool] = useState(false);
    const [diller,setDiller] = useState("");
    const [dillerBool,setDillerBool] = useState(false);
    const [markets,setMarkets] = useState([])

    useEffect(()=>{
        menuBool === true ? setMenu("active") : setMenu("");
    },[menuBool])

    useEffect(()=>{
        kategoryBool === true ? setKategory("active") : setKategory("");
    },[kategoryBool])

    useEffect(()=>{
      dillerBool === true ? setDiller("active") : setDiller("");
    },[dillerBool])

    const SearchHandler = (e)=>{
        e.preventDefault();
        setSearchInput("")
        console.log("searchData",searchData);
        history.push({
          pathname:"/search/"+searchData
        })
    }
    useEffect(()=>{
      let welayatId = JSON.parse(localStorage.getItem("welayatId"));
      getMarkets(welayatId);
    },[])

    const getMarkets = (welayatId)=>{
      axiosInstance.get("/api/kategoryOfMarkets/"+welayatId).then((data)=>{
        setMarkets(data.data);
        console.log(data.data);
      }).catch((err)=>{
        console.log(err);
      })
    }

    const GoToMarket = (id)=>{
      localStorage.setItem("MarketId",id);
      history.push({
        pathname:"/market/"+id,
      })
    }
    return(
        <header style={{position:"sticky",top:"0px",zIndex:"1000"}} className="header">
        <div className="header__container __container">
          <Link to="/home" class="header__logo logo">
            <img src={logo} alt="logo" />
            <div className="logo__name">Çynar</div>
          </Link>
          <menu  className="header__menu menu ">
            <div onClick={()=>setMenuBool(!menuBool)} className={`menu__burger ${menu}`}>
              <span></span>
              <span></span>
              <span></span>
            </div>
            <div className={`menu__body ${menu}`}>
              <ul className="menu__list" style={{paddingTop:"10px"}}>
                <li className="menu__first">
                  <Link to="/home" className="menu__link menu__link--active">
                    <p data-lang="home">{dil==="TM"?("Baş sahypa"):(dil==="RU"?"Дом":"Home")}</p>
                  </Link>
                </li>
                <li className="menu__first">
                  <Link onClick={()=>setKategoryBool(!kategoryBool)}  to="#" class="menu__link">
                    <p data-lang="products">{dil==="TM"?("Magazynlar"):(dil==="RU"?"Маркеты":"Markets")}</p> 
                  </Link>
                  {/* <!-- ikinkji menyu mobilkada cykar yaly hokman gosmaly  --> */}
                  <span onClick={()=>setKategoryBool(!kategoryBool)}  className="menu__second-arrow"></span>
                  <ul className={`menu__second ${kategory}`}>
                    {
                      markets && markets.map((markett)=>{
                        return markett.Markets.map((market)=>{
                          return market.active &&  <li onClick={()=>GoToMarket(market.id)}>
                                      <Link style={{minWidth:"100px"}} 
                                      // to={`/market/${market.id}`}
                                      ><p>{dil==="TM"?market.name_tm:(dil==="RU"?market.name_ru:market.name_en)}</p></Link>
                                </li>
                        })
                       
                      })
                    }
                    
                  </ul>
                </li>
                <li className="menu__first">
                  <Link to="/brends"  className="menu__link __contact-us-popup">
                    <p data-lang="contact_us">{dil==="TM"?"Brendler":(dil==="RU"?"Бренды":"Brends")}</p>
                  </Link>
                </li>
                <li className="menu__first">
                  <Link to="/"  className="menu__link __contact-us-popup">
                    <p data-lang="contact_us">{dil==="TM"?`Şäherler`:(dil==="RU"?`Городa`:`Cities`)}</p>
                  </Link>
                </li>
              </ul>
            </div>
          </menu>
          <div className="header__icons">
            <div className="header__icon js-search">
              <button onClick={()=>setSearchInput("active")}>
                <img src={search} alt="search" />
              </button>
            </div>
            <div className="header__icon">
              <Link to="/profile"
                ><img src={user} alt="user"
              /></Link>
            </div>
            <div className="header__icon">
              <Link to="/favourites">
                <img
                  src={heart}
                  alt="shopping-cart"
                />
              </Link>
            </div>
            <div className="header__icon">
              <Link className="SebetIcon" to="/basket">
                <img
                  src={shoppingCart}
                  alt="shopping-cart"
                />
                {sany>0?<span style={{backgroundColor:"#f0f2f5",color:"#dd7437",fontWeight:"900",fontSize:"16px",borderRadius:"50%",padding:"3px"}}>{sany}</span>:null}
              </Link>
            </div>
            <div className="header__icon header__lang lang ">
              <span className="lang__current " onClick={()=>setDillerBool(!dillerBool)} id="current-lang">{dil}</span>
              <ul className={`lang__menu ${diller}`}>
                {dil==="TM"?
                <li value="TM" onClick={()=>{ChangeDil("TM");setDillerBool(!dillerBool);}} className="active"><p>Türkmen</p></li>
                :<li value="TM" onClick={()=>{ChangeDil("TM");setDillerBool(!dillerBool);}} ><p>Türkmen</p></li>}
                {dil==="RU"?
                <li value="RU" onClick={()=>{ChangeDil("RU");setDillerBool(!dillerBool);}} className="active" ><p>Rus</p></li>
                :<li value="RU" onClick={()=>{ChangeDil("RU");setDillerBool(!dillerBool);}} ><p>Rus</p></li>}
                {dil === "EN"?
                <li value="EN" onClick={()=>{ChangeDil("EN");setDillerBool(!dillerBool);}} className="active"><p>Iňlis</p></li>
                :<li value="EN" onClick={()=>{ChangeDil("EN");setDillerBool(!dillerBool);}}><p>Iňlis</p></li>}
              </ul>
            </div>
          </div>
          <section className={`header__search search ${SearchInput}`}>
            <form onSubmit={(e)=>SearchHandler(e)} className="search__form">
              <img onClick={(e)=>SearchHandler(e)} src={search} alt="search" />
              <input onChange={(e)=>setSearchData(e.target.value)} type="text" placeholder="Search" />
            </form>
            <button onClick={()=>setSearchInput("")} className="search__close js-search">
              <svg
                id="cancel_icn_pink"
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
              >
                <rect id="bg" width="24" height="24" fill="none" />
                <path
                  id="plus"
                  d="M0,0,11.314,11.314"
                  transform="translate(6.343 6.343)"
                  fill="none"
                  stroke="#ff9105"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-miterlimit="10"
                  stroke-width="2"
                />
                <path
                  id="plus-2"
                  data-name="plus"
                  d="M11.314,0,0,11.314"
                  transform="translate(6.343 6.343)"
                  fill="none"
                  stroke="#ff9105"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-miterlimit="10"
                  stroke-width="2"
                />
              </svg>
            </button>
          </section>
        </div>
      </header>
    )
}

export default Header;