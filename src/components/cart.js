import React,{useContext, useEffect, useState} from "react";
import { Link, useHistory } from "react-router-dom";

import "../css/style.css";

import heart from "../img/icons/heart.svg";
import heartLiked from "../img/icons/heart-liked.svg";
import serpay from "../img/main/serpay.png";
import shoppingCart from "../img/icons/shopping-cart.svg"
import { BASE_URL } from "../utils/axiosIntance";
import { SebedimContext } from "../context/Sebedim";
import { message } from "antd";
import { LazyLoadImage } from 'react-lazy-load-image-component';

const Cart = (props)=>{
    
    const history = useHistory();
    const {dil,AddTo,Decrement,AddToFav} = useContext(SebedimContext);
    const [number,setNumber] = useState(0);
    const [orderBool,setOrderBool] = useState(false);
    const [order,setOrder] = useState("");
    const [buttons,setButtons] = useState("");
    const [like,setLike] = useState("")

    useEffect(()=>{
        orderBool === true ? setOrder("close") : setOrder("");
        orderBool === true ? setButtons("active") : setButtons("");
        let time = setTimeout(()=>{
            setOrderBool(false)
        },3000)
        return () => clearTimeout(time)
    },[orderBool]);

    const iLike = (product)=>{
        if(like==="added"){
            setLike("")
            AddToFav(product)
        }else{
            setLike("added");
            AddToFav(product)
        }
    }

    const IncrementCart = (product)=>{
      if(product.total_amount>=number){
        setNumber(number+1);
        AddTo(product);
      }else{
        message.warn("Ambarda mundan köp ýok!")
      }
    }
    const DecrimentCart = (product)=>{
      if(number>0){
        setNumber(number-1);
        Decrement(product.id)
      }
    }
    return(
        <article
        //  onClick={()=>props.onClick(props?.product.id)} 
        key={"product"+props?.product.id} class="product__item">
          {props?.product?.is_new && <div className="product_sale">
                 <span>{dil==="TM"?"Taze Haryt":(dil==="RU"?"Новый продукт":"New Product")}</span>
          </div>}
          {!props?.product?.is_new && props?.product.is_sale && <div className="product_sale">
                 <span>10%</span>
          </div>}
        <button onClick={()=>iLike(props.product)} class={`product__like like ${like} ${props.liked}`}>
          <img
            src={heart}
            alt="heart"
            class="no-liked"
          />
          <img
            src={heartLiked}
            alt="heart"
            class="liked"
          />
        </button>
        <div onClick={()=>history.push({pathname:`/product/${props.product.id}`})} class="product__img">
          {/* <img src={BASE_URL+"/"+props.product.surat}  alt="surat" loading="lazy"/> */}
          <LazyLoadImage
            alt={"surat"}
            // height={image.height}
            src={BASE_URL+"/"+props.product.surat} // use normal <img> attributes as props
            // width={image.width}
            // placeholderSrc={serpay}
             />

        </div>
        <div class="product__inner-box">
          <div class="product__price">{props.product.is_sale ===true?
           (props.product.is_valyuta_price?props.product.Config.currency_exchange*props.product.sale_price:props.product.sale_price)
           : (props.product.is_valyuta_price?props.product.Config.currency_exchange*props.product.price:props.product.price)} <span>TMT</span></div>
          <div class={`product-counter product__count ${buttons}`}>
            <button  onClick={()=>{DecrimentCart(props.product)}} class="product-counter__count-dec">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="7.104"
                height="2.856"
                viewBox="0 0 7.104 2.856"
              >
                <path
                  id="Path_343"
                  data-name="Path 343"
                  d="M823.406,881.072a1.481,1.481,0,0,1-1.056-.408,1.4,1.4,0,0,1-.408-1.032,1.369,1.369,0,0,1,.408-1.008,1.481,1.481,0,0,1,1.056-.408h4.176a1.475,1.475,0,0,1,1.464,1.464,1.331,1.331,0,0,1-.432,1.008,1.446,1.446,0,0,1-1.032.384Z"
                  transform="translate(-821.942 -878.216)"
                  fill="rgba(0,0,0,0.87)"
                />
              </svg>
            </button>
            <input type="number" value={number} />
            <span>{dil==="TM"?(props.product?.Unit?.name_tm):(dil==="RU"?props.product?.Unit?.name_ru:props.product?.Unit?.name_en)}</span>
            <button onClick={()=>IncrementCart(props.product)} class="product-counter__count-inc">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="12.456"
                height="12.432"
                viewBox="0 0 12.456 12.432"
              >
                <path
                  id="Path_342"
                  data-name="Path 342"
                  d="M757.635,877.6a1.3,1.3,0,0,1,.96.384,1.224,1.224,0,0,1,.384.936,1.3,1.3,0,0,1-.384.96,1.349,1.349,0,0,1-.96.36h-3.36v3.144a1.454,1.454,0,0,1-.456,1.1,1.5,1.5,0,0,1-1.1.432,1.577,1.577,0,0,1-1.128-.432,1.5,1.5,0,0,1-.432-1.1V880.24h-3.288a1.312,1.312,0,0,1-1.344-1.344,1.224,1.224,0,0,1,.384-.936,1.349,1.349,0,0,1,.96-.36h3.288v-3.576a1.5,1.5,0,0,1,.432-1.1,1.609,1.609,0,0,1,1.152-.432,1.479,1.479,0,0,1,1.536,1.536V877.6Z"
                  transform="translate(-746.523 -872.488)"
                  fill="#fff"
                />
              </svg>
            </button>
          </div>
          <div class={`product__go-to-trash ${order}`}>
           {!(props?.product?.Renklers[0] || props?.price?.Razmerlers[0]) && <button onClick={()=>setOrderBool(true)}>
              <img
                src={shoppingCart}
                alt="shopping-cart"
              />
            </button>}
          </div>
        </div>
        <Link to={`/product/${props.product.id}`} class="product__name">{dil==="TM"?(props.product.name_tm):(dil==="RU"?props.product.name_ru:props.product.name_en)}</Link>
      </article>
    )
}

export default Cart;