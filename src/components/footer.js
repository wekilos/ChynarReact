import React, { useContext, useEffect, useState } from "react";

import {Link, useParams} from "react-router-dom";

import logo from '../img/icons/logo.svg';
import appStore from "../img/app-store.png";
import location from "../img/icons/footer/location.svg"
import clock from "../img/icons/footer/clock.svg";
import phone from "../img/icons/footer/phone.svg";
import playMarket from "../img/google-play.png"
import "../css/style.css";
import { axiosInstance, BASE_URL } from "../utils/axiosIntance";
import { SebedimContext } from "../context/Sebedim";
const Footer = ()=>{
  const {idM} = useParams();
  const marketId = localStorage.getItem("MarketId") 
  const {dil} = useContext(SebedimContext)
  const [market,setMarket] = useState(null);
 useEffect(()=>{
  let MarketId = idM || 3;
   getData(MarketId);
 },[idM]);
 

  const getData = (MarketId)=>{
      // let MarketId = localStorage.getItem("MarketId");
      axiosInstance.get("/api/market/"+MarketId ).then((data)=>{
        console.log("footer",data.data);
        setMarket({...data.data});
      }).catch((err)=>{
        console.log(err);
      })
  }
    return(
      <footer className="footer">
      <div className="footer__container __container">
        <section className="footer__top">
          <section className="footer__about">
            <Link to={`/market/${market?.id}`}  className="footer__logo logo">
              <img src={market && BASE_URL+"/"+market.surat} alt="logo" />
              <div className="logo__name">{market && dil==="TM"?market.name_tm:(dil==="RU"?market && market.name_ru:market && market.name_en)}</div>
            </Link>
            <p className="footer__about-text">
              {market && dil==="TM"?market.description_tm:(dil==="RU"?market && market.description_ru: market &&  market.description_en)}
            </p>
          </section>
          <section className="footer__contacts">
            <h3 className="footer__contacts-title">{dil==="TM"?"Biz bilen habarlaşyň":(dil==="RU"?"Связаться с нами":"Contact us")}</h3>
            <ul>
              <li>
                <div className="footer__contacts--icons">
                  <img src={location} alt="" />
                </div>
                <p>{market && dil==="TM"?market.addres_tm:(dil==="RU"?market && market.addres_ru:market && market.addres_en)}</p>
              </li>
              <li>
                <div className="footer__contacts--icons">
                  <img src={clock} alt="" />
                </div>
                <p>{market && market.dastawkaStartI && market.dastawkaStartI.slice(0,5)} - {market && market.dastawkaEndII && market.dastawkaEndII.slice(0,5)}</p>
              </li>
              <li>
                <div className="footer__contacts--icons">
                  <img src={phone} alt="" />
                </div>
                <p>+{market && market.tel}</p>
              </li>
            </ul>
          </section>
          <section className="footer__app-links">
            <div className="footer__app-links--row">
              <Link to="#" target="_blank" rel="noopener noreferrer">
                <img src={playMarket} alt="" />
              </Link>
              <Link to="#" target="_blank" rel="noopener noreferrer">
                <img src={appStore} alt="" />
              </Link>
            </div>
            <div className="footer__app-links--question">
              <p>
                {dil==="TM"?"Siziň soragyňyz galan bolsa biz bilen":(dil==="RU"?"Если у вас есть вопросы, не стесняйтесь обращаться к нам":"If you have any questions, feel free to contact us")}
                <Link to="/contact">{dil==="TM"?" habarlaşyp bilersiňiz":(dil==="RU"?" вы можете связаться":" you can contact")}</Link>
              </p>
            </div>
          </section>
        </section>
      </div>
      <div className="footer__bottom">
        <p>POWERED BY WB - © 2021. ALL RIGHTS RESERVED.</p>
      </div>
    </footer>
    )
}

export default Footer;