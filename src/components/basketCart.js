import { message } from "antd";
import React, { useContext, useState } from "react";
import { Link,useHistory } from "react-router-dom";

import "../css/style.css";

import surat from "../img/products/1.png";

import { SebedimContext } from "../context/Sebedim";
import { BASE_URL } from "../utils/axiosIntance";

const BasketCart = (props)=>{

  const [incBool,setIncBool] = props.bool;
    const history = useHistory();
    const {dil,Decrement,AddTo,Remove} = useContext(SebedimContext);

    const [number,setNumber] = useState(props.product.sany);
    const [product,setProduct] = useState(props.product);
    let price = props.product.product.is_valyuta_price?
    (props.product.product.is_sale?props.product.product.sale_price*props.product.product.Config.currency_exchange:props.product.product.price*props.product.product.Config.currency_exchange)
    :(props.product.product.is_sale?props.product.product.sale_price:props.product.product.price);
    const [productPrice,setProductPrice] = useState(price)

    const Decriment = (id)=>{
      number>1&& setNumber(number-1)
      number>1&& Decrement(id);
      number>1&& setIncBool(!incBool)
    }

    const Increment = (product) =>{
      console.log(product)
      if(product.total_amount>number){
        setNumber(number+1);
        AddTo(product)
        setIncBool(!incBool)
      }else{
        message.warn("Bagyşlaň artyk alyp bileňizok!")
      }
    }

    const Delete = ()=>{
      Remove(product.id)
      setIncBool(!incBool);
    }

    
    console.log("cart prooo",props.product)
    return(
        <tr>
                  <td>
                    <div onClick={()=>history.push({pathname:`/product/${product.product.id}`})} class="basket__img">
                      <img src={BASE_URL+"/"+product.product.surat} alt="surat" />
                    </div>
                  </td>
                  <td>
                    <div class="basket__content">
                      <Link to={`/product/${product.product.id}`} class="basket__product-title">
                          {dil==="TM"?product.product.name_tm:(dil==="RU"? product.product.name_ru:product.product.name_en )} </Link>
                      <div class="basket__product-model">Model № {product.product.id}</div>
                    </div>
                  </td>
                  <td>
                    <div class="basket__content">
                      <Link class="basket__product-title">
                      {product.razmer && (dil==="TM"?"Razmer: ":(dil==="RU"?"Размер":"Size")) } {product.razmer} </Link>
                      <div class="basket__product-model">{product.renk && (dil==="TM"?"Renk: ":(dil==="RU"?"Цвет":"Color")) } {product.renk}</div>
                    </div>
                  </td>
                  <td>
                    <div class="basket_counter product-counter">
                      <button onClick={()=>Decriment(product.product.id)} class="product-counter__count-dec">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="7.104"
                          height="2.856"
                          viewBox="0 0 7.104 2.856"
                        >
                          <path
                            id="Path_343"
                            data-name="Path 343"
                            d="M823.406,881.072a1.481,1.481,0,0,1-1.056-.408,1.4,1.4,0,0,1-.408-1.032,1.369,1.369,0,0,1,.408-1.008,1.481,1.481,0,0,1,1.056-.408h4.176a1.475,1.475,0,0,1,1.464,1.464,1.331,1.331,0,0,1-.432,1.008,1.446,1.446,0,0,1-1.032.384Z"
                            transform="translate(-821.942 -878.216)"
                            fill="rgba(0,0,0,0.87)"
                          />
                        </svg>
                      </button>
                      <input type="number" value={number} />
                      <span>{product.product.Unit && dil==="TM"?product.product.Unit.name_tm:(dil==="RU"?product.product.Unit.name_ru:product.product.Unit.name_en)}</span>
                      <button onClick={()=>Increment(product.product)} class="product-counter__count-inc">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="12.456"
                          height="12.432"
                          viewBox="0 0 12.456 12.432"
                        >
                          <path
                            id="Path_342"
                            data-name="Path 342"
                            d="M757.635,877.6a1.3,1.3,0,0,1,.96.384,1.224,1.224,0,0,1,.384.936,1.3,1.3,0,0,1-.384.96,1.349,1.349,0,0,1-.96.36h-3.36v3.144a1.454,1.454,0,0,1-.456,1.1,1.5,1.5,0,0,1-1.1.432,1.577,1.577,0,0,1-1.128-.432,1.5,1.5,0,0,1-.432-1.1V880.24h-3.288a1.312,1.312,0,0,1-1.344-1.344,1.224,1.224,0,0,1,.384-.936,1.349,1.349,0,0,1,.96-.36h3.288v-3.576a1.5,1.5,0,0,1,.432-1.1,1.609,1.609,0,0,1,1.152-.432,1.479,1.479,0,0,1,1.536,1.536V877.6Z"
                            transform="translate(-746.523 -872.488)"
                            fill="#fff"
                          />
                        </svg>
                      </button>
                    </div>
                  </td>
                  <td>
                    <div class="basket__product-price">{productPrice*number} TMT</div>
                  </td>
                  <td>
                    <button onClick={()=>Delete()} class="basket__product-delete">
                      <svg
                        id="Icons_cancel_icn"
                        data-name="Icons/cancel_icn"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                      >
                        <g id="cancel_icn">
                          <rect id="bg" width="24" height="24" fill="none" />
                          <path
                            id="plus"
                            d="M0,0,11.314,11.314"
                            transform="translate(6.343 6.343)"
                            fill="none"
                            stroke="#d1d1d6"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-miterlimit="10"
                            stroke-width="2"
                          />
                          <path
                            id="plus-2"
                            data-name="plus"
                            d="M11.314,0,0,11.314"
                            transform="translate(6.343 6.343)"
                            fill="none"
                            stroke="#d1d1d6"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-miterlimit="10"
                            stroke-width="2"
                          />
                        </g>
                      </svg>
                    </button>
                  </td>
                </tr>
    )
}

export default BasketCart;