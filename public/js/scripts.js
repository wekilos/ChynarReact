
$('.__js-slider').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 5000,
})
$('.__js-slider-3').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1025,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
})

$('.reg-inputs').autotab({ format: 'number' })

// var swiper = new Swiper('.product-slider__vertical', {
//   loop: false,
//   slidesPerView: null,
//   freeMode: true,
//   watchSlidesVisibility: true,
//   watchSlidesProgress: true,
//   direction: 'vertical',
//   noSwiping: false,
//   autoplay: false,
//   keyboard: false,
// })
// var swiper2 = new Swiper('.product-slider__horizontal', {
//   loop: false,
//   slidesPerView: 1,
//   spaceBetween: 10,
//   navigation: {
//     nextEl: '.swiper-button-next',
//     prevEl: '.swiper-button-prev',
//   },
//   thumbs: {
//     swiper: swiper,
//   },
// })

$('#p-hidden').click(function () {
  $('.p-password').attr('type', 'text')
  $(this)[0].style.display = 'none'
  $('#p-visible')[0].style.display = 'block'
})

$('#p-visible').click(function () {
  $('.p-password').attr('type', 'password')
  $(this)[0].style.display = 'none'
  $('#p-hidden')[0].style.display = 'block'
})




$('.p-category__title').click(function (event) {
  $(this).toggleClass('active')
  if ($(this).hasClass('active')) {
    $('.p-category__row').slideDown()
  } else {
    $('.p-category__row').slideUp()
  }
})

$('.-js-popup').click(function (event) {
  $('.payment-popup').toggleClass('active')
})

$('.like').click(function (event) {
  $(this).toggleClass('added')
})

$('.profile__controllers-menu').click(function (event) {
  $(this).toggleClass('active')
  if ($(this).hasClass('active')) {
    $('.profile__controllers-box').slideDown()
  } else {
    $('.profile__controllers-box').slideUp()
  }
})

$('.product__go-to-trash').click(function (event) {
  $(this).addClass('close')
  $(this).parent().find('.product__count').addClass('active')
  setTimeout(() => {
    $(this).removeClass('close')
    $(this).parent().find('.product__count').removeClass('active')
  }, 2000)
})


function numbered() {
  $('.product__count').each(function (i) {
    $(this).attr('index', i)
  })
}
numbered()

let timeOut = new Array($('.product__count').length)
$(
  '.product__count > .product-counter__count-dec, .product__count > .product-counter__count-inc'
).click(function (event) {
  $(this).parent().css('cssText', 'display: flex')
  $(this).parent().parent().find('.product__go-to-trash').css('display', 'none')
  let thisIndex = parseInt($(this).parent().attr('index'))
  window.clearTimeout(timeOut[thisIndex])
  timeOut[thisIndex] = window.setTimeout(() => {
    $(this).parent().css('display', '')
    $(this).parent().parent().find('.product__go-to-trash').css('display', '')
  }, 2000)
})

$('.product__count > input')
  .focusin(function (event) {
    $(this).parent().css('cssText', 'display: flex')
    $(this)
      .parent()
      .parent()
      .find('.product__go-to-trash')
      .css('display', 'none')
    let thisIndex = parseInt($(this).parent().attr('index'))
    window.clearTimeout(timeOut[thisIndex])
  })
  .focusout(function (event) {
    let thisIndex = parseInt($(this).parent().attr('index'))
    timeOut[thisIndex] = window.setTimeout(() => {
      $(this).parent().css('display', '')
      $(this).parent().parent().find('.product__go-to-trash').css('display', '')
    }, 2000)
  })

$('.product-counter__count-dec').click(function (event) {
  let value = $(this).parent().find('input')
  if (value.val() > 0) {
    $(value).val(parseFloat(value.val()) - 1)
  }
})

$('.product-counter__count-inc').click(function (event) {
  let value = $(this).parent().find('input')
  $(value).val(parseFloat(value.val()) + 1)
})



// popup
// $('.__contact-us-popup').click(function (event) {
//   $('.contact-us-popup').addClass('active')
// })

// window.addEventListener('click', function (e) {
//   if (
//     !(
//       $('.contact-us-popup__wrapper')[0].contains(e.target)
//         ||
//       $('.__contact-us-popup')[0].contains(e.target))
//   ) {
//     $('.contact-us-popup').removeClass('active')
//   }
// })

// timer 
function countdown() {
  if ($('.countdown').length > 0) {  
    let day = parseInt($('.countdown__index>span')[0].innerText)
    let hour = parseInt($('.countdown__index>span')[1].innerText)
    let minut = parseInt($('.countdown__index>span')[2].innerText)
      setInterval(function () {
        minut--
      if (minut < 0) {
        minut = 59
        hour--
      }
      if (hour < 0) {
        hour = 23
        day--
      }
      if (day < 0) {
        day = 0
        $('.countdown').remove()
      }
      $('.countdown__index>span')[0].innerText = day
      $('.countdown__index>span')[1].innerText = hour
      $('.countdown__index>span')[2].innerText = minut
      
    }, 60000)
  }
}
countdown()